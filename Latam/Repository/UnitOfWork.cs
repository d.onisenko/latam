﻿using Latam.Model;
using Latam.Model.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace Latam.Repository {
    public class UnitOfWork : IUnitOfWork, IDisposable {
        private readonly LatamContext _context;
        private readonly IConfiguration _configuration;

        private readonly MethodInfo _repositoryMethod;
        private readonly Dictionary<Type, object> _repositories = new Dictionary<Type, object>();

        public UnitOfWork(LatamContext context, IConfiguration configuration) {
            _context = context;
            _configuration = configuration;
            _repositoryMethod = GetType().GetMethod("Repository", new Type[0]);
        }

        #region Entities

        public IRepository<Product> Product => Repository<Product>();
        public IRepository<Category> Category => Repository<Category>();
        public IRepository<Purpose> Purpose => Repository<Purpose>();
        public IRepository<Shop> Shop => Repository<Shop>();
        public IRepository<ProductInOrder> ProductInOrder => Repository<ProductInOrder>();
        public IRepository<OrderStatus> OrderStatus => Repository<OrderStatus>();
        public IRepository<Order> Order => Repository<Order>();
        public IRepository<LatamUser> LatamUser => Repository<LatamUser>();
        public IRepository<VisitorsCount> VisitorsCount => Repository<VisitorsCount>();

        #endregion

        public virtual IDbConnection CreateDbConnection() =>
            new SqlConnection(_configuration.GetConnectionString("LatamDb"));

        public void Dispose() =>
            _context.Dispose();

        public IRepository<TEntity> Repository<TEntity>()
            where TEntity : class, new() =>
            _repositories.TryGetValue(typeof(TEntity), out object repository)
                ? (IRepository<TEntity>)repository
                : (IRepository<TEntity>)(_repositories[typeof(TEntity)] = new EntityRepository<TEntity>(_context));

        public IRepository Repository(Type entityType) =>
            (IRepository)_repositoryMethod.MakeGenericMethod(entityType).Invoke(this, null);
    }
}
