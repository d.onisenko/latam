﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Latam.Repository {
    public interface IRepository<TEntity> : IQueryable<TEntity>, IRepository
        where TEntity : class, new() {

        new TEntity Get(object id);
        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> condition);

        int Add(TEntity item);
        int Add(IEnumerable<TEntity> items);

        int Update(TEntity item);
        int Update(Action<TEntity> updateAction, Expression<Func<TEntity, bool>> condition);

        int Delete(TEntity item);
        int Delete(Expression<Func<TEntity, bool>> condition);

        void Update(IEnumerable<TEntity> originalItems, IEnumerable<TEntity> modifiedItems,
            Func<TEntity, TEntity, bool> matchCondition, Action<TEntity, TEntity> updateAction);
    }

    public interface IRepository {
        object Get(object id);
        IEnumerable<object> Get(Expression<Func<object, bool>> condition);
        int Add(object item);
        int Add(IEnumerable<object> items);

        int Update(object item);
        int Update(Action<object> updateAction, Expression<Func<object, bool>> condition);

        int Delete(object item);
        int Delete(Expression<Func<object, bool>> condition);
    }
}
