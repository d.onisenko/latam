﻿using Latam.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Latam.Repository {
    public class EntityRepository<TEntity> : IRepository<TEntity>, IRepository
        where TEntity : class, new() {

        private readonly LatamContext _context;
        private DbSet<TEntity> _itemsInternal;

        public EntityRepository(LatamContext context) {
            _context = context;
        }

        private DbSet<TEntity> ItemsInternal =>
            _itemsInternal ?? (_itemsInternal = _context.Set<TEntity>());

        #region IQueryable

        public Type ElementType =>
            (ItemsInternal as IQueryable).ElementType;

        public Expression Expression =>
            (ItemsInternal as IQueryable).Expression;

        public IQueryProvider Provider =>
            (ItemsInternal as IQueryable).Provider;

        public IEnumerator<TEntity> GetEnumerator() =>
            (ItemsInternal as IQueryable<TEntity>).GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() =>
            GetEnumerator();

        #endregion

        #region IRepository<TEntity>

        public TEntity Get(object id) =>
            ItemsInternal.Find(id);
        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> condition) =>
            ItemsInternal.Where(condition);

        public int Add(TEntity item) {
            ItemsInternal.Add(item);
            return _context.SaveChanges();
        }
        public int Add(IEnumerable<TEntity> items) {
            ItemsInternal.AddRange(items);
            return _context.SaveChanges();
        }

        public int Update(TEntity item) {
            ItemsInternal.Update(item);
            return _context.SaveChanges();
        }
        public int Update(Action<TEntity> updateAction, Expression<Func<TEntity, bool>> condition) {
            var items = Get(condition);
            items.ForEach(updateAction);
            return _context.SaveChanges();
        }

        public int Delete(TEntity item) {
            ItemsInternal.Remove(item);
            return _context.SaveChanges();
        }

        public int Delete(Expression<Func<TEntity, bool>> condition) {
            var items = Get(condition);
            ItemsInternal.RemoveRange(items);
            return _context.SaveChanges();
        }

        public void Update(IEnumerable<TEntity> originalItems, IEnumerable<TEntity> modifiedItems,
            Func<TEntity, TEntity, bool> matchCondition, Action<TEntity, TEntity> updateAction) {
            foreach (var originalItem in originalItems) {
                var modified = modifiedItems.FirstOrDefault(p => matchCondition(p, originalItem));
                if (modified != null) {
                    updateAction(originalItem, modified);
                }
            }

            _context.SaveChanges();
        }

        #endregion

        #region IRepository

        object IRepository.Get(object id) =>
            ItemsInternal.Find(id);
        IEnumerable<object> IRepository.Get(Expression<Func<object, bool>> condition) =>
            throw new NotImplementedException();

        public IQueryable<object> AsQueryable() {
            return ItemsInternal.AsQueryable();
        }

        int IRepository.Add(object item) =>
            Add((TEntity)item);
        int IRepository.Add(IEnumerable<object> items) =>
            Add(items.OfType<TEntity>());

        int IRepository.Update(object item) =>
            Update((TEntity)item);
        int IRepository.Update(Action<object> updateAction, Expression<Func<object, bool>> condition) =>
            throw new NotImplementedException();

        int IRepository.Delete(object item) =>
            Delete((TEntity)item);
        int IRepository.Delete(Expression<Func<object, bool>> condition) =>
            throw new NotImplementedException();

        #endregion
    }
}
