﻿using Latam.Model.Entities;
using System;
using System.Data;

namespace Latam.Repository {
    public interface IUnitOfWork : IDisposable {
        IDbConnection CreateDbConnection();
        IRepository<TEntity> Repository<TEntity>()
             where TEntity : class, new();
        IRepository Repository(Type entityType);

        IRepository<Product> Product { get; }
        IRepository<Purpose> Purpose { get; }
        IRepository<Category> Category { get; }
        IRepository<Shop> Shop { get; }
        IRepository<ProductInOrder> ProductInOrder { get; }
        IRepository<OrderStatus> OrderStatus { get; }
        IRepository<Order> Order { get; }
        IRepository<LatamUser> LatamUser { get; }
        IRepository<VisitorsCount> VisitorsCount { get; }
    }
}
