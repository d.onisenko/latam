import { AfterViewChecked, AfterViewInit, Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Product } from "../model/product";
import { ProductDto } from "../model/product-dto";
import { CartService } from "../utils/cart.service";
import { ServiceUtils } from "../utils/service-utils";

@Component({
    selector: "product-page",
    templateUrl: "./product-page.component.html",
    styleUrls: ["./product-page.component.css"],
})
export class ProductComponent implements AfterViewChecked {
    private _id: number;

    public imageSource: string;

    public product: Product;
    public count: number = 1;

    constructor(
        private _serviceUtils: ServiceUtils,
        private _cartService: CartService,
        public _route: ActivatedRoute
    ) {}

    ngAfterViewChecked(): void {
        this._route.queryParams.subscribe((queryParams) => {
            let newId = queryParams["id"];
            if (newId === this._id) {
                return;
            }
            this._id = newId;
            this._serviceUtils
                .get<Product>(`Product/GetById/?id=${this._id}`)
                .subscribe((response) => {
                    this.product = response;
                    this.product.pictureUrl = [];
                    let picName = this.product.nameEn
                        .toLowerCase()
                        .replace(/\s/g, "");
                    this.product.pictureUrl.push(
                        `assets\\production\\${picName}_1.jpg`
                    );
                    this.product.pictureUrl.push(
                        `assets\\production\\${picName}_2.jpg`
                    );
                    this.imageSource = this.product.pictureUrl[0];

                    window.sessionStorage.setItem(
                        "productId",
                        this._id.toString()
                    );
                });
        });
    }

    onBuy() {
        if (!this.count) {
            return;
        }
        let productDto = new ProductDto(this.product.id, this.count);
        this._cartService.add(productDto);
    }

    onPictureClick() {
        let elements = document.getElementsByClassName("product-page-image");
        if (elements) {
            for (let index = 0; index < elements.length; index++) {
                const element = elements[index];
                if (!element.className.includes("invisible")) {
                    element.classList.remove("dissappear");
                    element.classList.remove("appear");
                    element.classList.add("dissappear");

                    setTimeout(() => {
                        element.classList.add("invisible");
                    }, 1500);
                } else {
                    setTimeout(() => {
                        element.classList.remove("invisible");
                        element.classList.remove("dissappear");
                        element.classList.remove("appear");

                        element.classList.add("appear");
                    }, 1500);
                }
            }
        }
    }

    getDisplay() {
        return window.innerWidth < 930 ? "grid" : "inline-flex";
    }
}
