import {
    Component,
    EventEmitter,
    HostListener,
    Input,
    Output,
    ViewChild,
} from "@angular/core";
import { Router } from "@angular/router";
import { AuthGuard } from "src/app/auth/auth.guard";
import { Auth } from "src/app/auth/auth.service";
import { Category } from "src/app/model/product";
import { SearchComponent } from "src/app/search/search-input/search.component";

@Component({
    selector: "nav-menu-slave",
    templateUrl: "./nav-menu-slave.component.html",
    styleUrls: ["./nav-menu-slave.component.css"],
})
export class NavMenuSlaveComponent {
    @Input()
    public categories: Category[];

    @Output()
    public onSelectEmitter: EventEmitter<string> = new EventEmitter<string>();

    @ViewChild(SearchComponent, { static: true })
    public searchComponent: SearchComponent;

    constructor(
        private _authGuard: AuthGuard,
        private _auth: Auth,
        private _router: Router
    ) {}

    onSelect(path: string, categoryId: number = null): void {
        if (path === "category") {
            let selectedCategory = this.categories.find(
                (c) => c.id === categoryId
            );
            if (selectedCategory) {
                path = `shop/category?ids=(${selectedCategory.id})`;
            }
        }
        this.onSelectEmitter.emit(path);
    }

    public isAdmin(): boolean {
        return this._authGuard.isAdmin();
    }

    public isLoggedIn(): boolean {
        return this._authGuard.isLoggedIn();
    }

    public getUserName(): string {
        return (
            (this._auth.currentUserValue &&
                this._auth.currentUserValue.login) ||
            ""
        );
    }

    public onLogoutClick(): void {
        this._auth.logout();
        this._router.navigateByUrl(`/`);
    }

    @HostListener("window:click", ["$event"])
    public onCLick(event: Event) {
        if (
            !event ||
            !event.target ||
            (event.target["id"] != "menu-nav-item" &&
                event.target["id"] != "nav-menu-img" &&
                event.target["id"] != "user-container-li" &&
                event.target["id"] != "user-container" &&
                event.target["id"] != "user-label" &&
                event.target["id"] != "logout-btn" &&
                event.target["id"] != "user-item" &&
                event.target["id"] != "user-img" &&
                event.target["id"] != "nav-menu-slave-container" &&
                (!event.target["parentElement"] ||
                    event.target["parentElement"]["id"] !=
                        "nav-menu-slave-container"))
        ) {
            this.onSelectEmitter.emit("");
        }
    }
}
