import { Component, HostListener, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { AboutSlaveComponent } from "../about-slave/about-slave.component";
import { Localizer } from "../localizer/localizer.service";
import { Category } from "../model/product";
import { SearchComponent } from "../search/search-input/search.component";
import { ShopSlaveComponent } from "../shop-slave/shop-slave.component";
import { ServiceUtils } from "../utils/service-utils";
import { CartService } from "../utils/cart.service";
import { Auth } from "../auth/auth.service";
import { AuthGuard } from "../auth/auth.guard";
import { EditSlaveComponent } from "../edit/edit-slave/edit-slave.component";
import { NavMenuSlaveComponent } from "./nav-menu-slave/nav-menu-slave.component";

@Component({
    selector: "app-nav-menu",
    templateUrl: "./nav-menu.component.html",
    styleUrls: ["./nav-menu.component.css"],
})
export class NavMenuComponent implements OnInit {
    public _isUk: boolean;
    public aboutSlaveVisible: boolean = false;
    public shopSlaveVisible: boolean = false;
    public editSlaveVisible: boolean = false;
    public navMenuSlaveVisible: boolean = false;
    public cartCount: number;
    public isToggleMode: boolean = false;

    public categories: Category[];

    @ViewChild(AboutSlaveComponent, { static: true })
    public aboutSlave: AboutSlaveComponent;

    @ViewChild(ShopSlaveComponent, { static: true })
    public shopSlave: ShopSlaveComponent;

    @ViewChild(EditSlaveComponent, { static: true })
    public editSlave: EditSlaveComponent;

    @ViewChild(NavMenuSlaveComponent, { static: true })
    public navMenuSlave: NavMenuSlaveComponent;

    @ViewChild(SearchComponent, { static: true })
    public searchComponent: SearchComponent;

    constructor(
        private _localizer: Localizer,
        private _router: Router,
        private _serviceUtils: ServiceUtils,
        private _cartService: CartService,
        private _auth: Auth,
        private _authGuard: AuthGuard
    ) {
        this._isUk = this._localizer.selectedCulture === "uk";
        this.cartCount = this._cartService.count;
        this._cartService.subscribeOnChanges(this, this.onCartChanges);
    }
    ngOnInit(): void {
        this.onResize(null);
        if (this.categories) {
            return;
        }
        this._serviceUtils
            .get<Category[]>("Category/Get")
            .subscribe((response) => {
                this.categories = response;
            });
    }

    onCartChanges(): void {
        this.cartCount = this._cartService.count;
    }

    onChangeLanguage() {
        this._localizer.setCulture();
        this._isUk = this._localizer.selectedCulture === "uk";
    }

    onAboutClick(): void {
        this.aboutSlaveVisible = !this.aboutSlaveVisible;
    }

    onShopClick(): void {
        this.shopSlaveVisible = !this.shopSlaveVisible;
    }

    onEditClick(): void {
        this.editSlaveVisible = !this.editSlaveVisible;
    }

    onNavMenuClick(): void {
        this.navMenuSlaveVisible = !this.navMenuSlaveVisible;
    }

    onAboutSlaveSelected(path: string): void {
        this.aboutSlaveVisible = false;
        if (path) {
            this._router.navigateByUrl(`/${path}`);
        }
    }

    onShopSlaveSelected(path: string): void {
        this.shopSlaveVisible = false;
        if (path) {
            this._router.navigateByUrl(`/${path}`);
        }
    }

    onEditSlaveSelected(path: string): void {
        this.editSlaveVisible = false;
        if (path) {
            this._router.navigateByUrl(`/${path}`);
        }
    }

    onMenuSlaveSelected(path: string): void {
        this.navMenuSlaveVisible = false;
        if (path) {
            this._router.navigateByUrl(`/${path}`);
        }
    }

    public getUserName(): string {
        return (
            (this._auth.currentUserValue &&
                this._auth.currentUserValue.login) ||
            ""
        );
    }

    public onLogoutClick(): void {
        this._auth.logout();
        this._router.navigateByUrl(`/`);
    }

    public isAdmin(): boolean {
        return this._authGuard.isAdmin();
    }

    public isLoggedIn(): boolean {
        return this._authGuard.isLoggedIn();
    }

    @HostListener("window:resize", ["$event"])
    public onResize(event: Event) {
        if (window.innerWidth < 1150) {
            this.isToggleMode = true;
        } else {
            this.isToggleMode = false;
        }
    }
}
