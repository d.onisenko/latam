export interface BaseWebResponse {
    isSuccess: boolean;
    message: string;
}
