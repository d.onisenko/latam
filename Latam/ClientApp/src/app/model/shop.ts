export class Shop {
    constructor() {
        this.nameEn = "";
        this.nameUk = "";
        this.changed = true;
    }

    id: number;
    nameEn: string;
    nameUk: string;

    changed: boolean;
    toRemove: boolean;
}
