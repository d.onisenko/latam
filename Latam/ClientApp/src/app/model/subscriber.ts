export class CartSubscriber {
    constructor(scope: object, handler: Function) {
        this.scope = scope;
        this.handler = handler;
    }

    scope: object;
    handler: Function;
}
