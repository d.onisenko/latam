export interface SearchDto {
    id: number;
    nameUk: string;
    nameEn: string;
    sectionName: string;
}
