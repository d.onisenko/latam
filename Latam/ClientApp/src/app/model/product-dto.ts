export class ProductDto {
    constructor(id: number, quantity: number) {
        this.id = id;
        this.quantity = quantity;
    }

    id: number;
    quantity: number;
}
