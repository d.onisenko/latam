export class Contact {
    name: string;
    surname: string;
    email: string;
    phone: string;
    comment: string;
}
