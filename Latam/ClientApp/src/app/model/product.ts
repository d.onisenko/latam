export interface Purpose {
    id: number;
    nameEn: string;
    nameUk: string;
}

export interface ProductInPurpose {
    id: number;
    productId: number;
    purposeId: number;

    purpose: Purpose;
}

export interface Category {
    id: number;
    nameEn: string;
    nameUk: string;
    description: string;
    isSelected: boolean;
}

export interface Product {
    id: number;
    nameEn: string;
    nameUk: string;
    createdOn: Date;
    descriptionEn: string;
    descriptionUk: string;
    price: number;
    categoryId: number;
    topNotesUk: string;
    middleNotesUk: string;
    baseNotesUk: string;
    topNotesEn: string;
    middleNotesEn: string;
    baseNotesEn: string;

    pictureUrl: string[];
    quantity: number;

    productInPurpose: ProductInPurpose[];
    category: Category;

    changed: boolean;
}
