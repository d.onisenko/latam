import { Product } from "./product";
import { Shop } from "./shop";

export class Order {
    id: number;
    createdOn: Date;
    totalPrice: number;
    isVisa: boolean;
    isCash: boolean;
    contactName: string;
    contactSurname: string;
    contactPhone: string;
    contactEmail: string;
    comment: string;
    isPickup: boolean;
    isNpPickup: boolean;
    isNpCourier: boolean;
    pickupShopId: number;
    city: string;
    npPickupNumber: string;
    street: string;
    house: string;
    flat: string;

    orderStatus: OrderStatus;
    pickupShop: Shop;
    productInOrder: ProductInOrder[];
}

export class OrderViewModel {
    constructor(order: Order) {
        this.order = order;
        this.dataColumns = [];
        this.dataColumns.push(new DataColumn("Номер", order.id));
        this.dataColumns.push(new DataColumn("Дата", order.createdOn));
        this.dataColumns.push(
            new DataColumn("Статус", order.orderStatus.nameUk)
        );

        let products = [];
        order.productInOrder.forEach((p) => {
            products.push(`${p.product.nameEn} (${p.quantity} шт.)`);
        });
        this.dataColumns.push(new DataColumn("Товары", products.join(", ")));

        this.dataColumns.push(new DataColumn("Сумма", order.totalPrice));
        this.dataColumns.push(new DataColumn("Оплата онлайн", order.isVisa));
        this.dataColumns.push(new DataColumn("Оплата наличными", order.isCash));
        this.dataColumns.push(new DataColumn("Имя", order.contactName));
        this.dataColumns.push(new DataColumn("Фамилия", order.contactSurname));
        this.dataColumns.push(new DataColumn("Телефон", order.contactPhone));
        this.dataColumns.push(new DataColumn("Email", order.contactEmail));
        this.dataColumns.push(new DataColumn("Комментарий", order.comment));
        this.dataColumns.push(new DataColumn("Самовывоз", order.isPickup));
        this.dataColumns.push(
            new DataColumn("Самовывоз из НП", order.isNpPickup)
        );
        this.dataColumns.push(new DataColumn("Курьер НП", order.isNpCourier));
        this.dataColumns.push(
            new DataColumn(
                "Магазин",
                (order.pickupShop && order.pickupShop.nameUk) || ""
            )
        );
        this.dataColumns.push(new DataColumn("Город", order.city));
        this.dataColumns.push(
            new DataColumn("НП номер отделения", order.npPickupNumber)
        );
        this.dataColumns.push(new DataColumn("Улица", order.street));
        this.dataColumns.push(new DataColumn("Дом", order.house));
        this.dataColumns.push(new DataColumn("Квартира", order.flat));
    }

    public order: Order;
    public dataColumns: DataColumn[];
    public changed: boolean;
}

export class DataColumn {
    private options: Intl.DateTimeFormatOptions;

    constructor(name: string, value: any) {
        this.name = name;
        let parsedValue = value.toString();
        if (value == null || value == undefined) {
            parsedValue = "";
        } else if (Number(value) === value) {
            parsedValue = value.toString();
        } else if (
            Boolean(value) === value ||
            ["true", "false"].includes(value.toString().toLowerCase())
        ) {
            parsedValue = value ? "Да" : "Нет";
        } else if (name === "Дата") {
            this.options = {
                year: "numeric",
                month: "2-digit",
                day: "2-digit",
                hour: "2-digit",
                minute: "2-digit",
            };
            parsedValue = new Date(value).toLocaleString("uk", this.options);
        }
        this.value = parsedValue;
    }

    public name: string;
    public value: string;
    public id: number;
}

export class ProductInOrder {
    id: number;
    orderId: number;
    productId: number;
    price: number;
    quantity: number;

    product: Product;
}

export interface OrderStatus {
    id: number;
    nameEn: string;
    nameUk: string;
}
