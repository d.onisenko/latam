export class LocalizerItem {
    public culture: string;
    public dictionary: object;
}

export class LocalizerResponse {
    public items: LocalizerItem[];
}
