import { Pipe, PipeTransform } from "@angular/core";
import { Localizer } from "./localizer.service";

@Pipe({
    name: "localize",
    pure: false,
})
export class LocalizePipe implements PipeTransform {
    constructor(private _localizer: Localizer) {}

    transform(value: string, object: Object = null) {
        return this._localizer.translate(value, object);
    }
}
