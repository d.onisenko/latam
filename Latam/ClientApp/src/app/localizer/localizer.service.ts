import { Injectable } from "@angular/core";
import { LocalizerItem, LocalizerResponse } from "../model/localizer-item";
import { ServiceUtils } from "../utils/service-utils";

@Injectable()
export class Localizer {
    private _dictionary: LocalizerResponse = null;
    private _selectedDictionary: LocalizerItem = null;
    private _initialized: boolean = false;
    private readonly _enCulture: string = "en";
    private readonly _ukCulture: string = "uk";

    public selectedCulture: string;

    constructor(private _serviceUtils: ServiceUtils) {
        let savedCulture = window.localStorage.getItem("selectedCulture");
        this.selectedCulture = savedCulture ? savedCulture : "uk";
    }

    public async init() {
        await this._serviceUtils
            .get<LocalizerResponse>("Translation/GetAll")
            .subscribe((result) => {
                this._dictionary = result;
                this._initialized = true;
                if (!this._selectedDictionary) {
                    this._selectedDictionary = result.items.find(
                        (r) => r.culture == this.selectedCulture
                    );
                }
            });
    }

    public translate(input: string, object: Object = null): string {
        if (object) {
            return object[
                input +
                    this.selectedCulture.charAt(0).toUpperCase() +
                    this.selectedCulture.slice(1)
            ];
        }

        if (!this._selectedDictionary) {
            return input;
        }

        if (!object) {
            return (
                this._selectedDictionary.dictionary[
                    Object.keys(this._selectedDictionary.dictionary).find(
                        (key) => key.toLowerCase() === input.toLowerCase()
                    )
                ] || input
            );
        }
    }

    public setCulture() {
        this.selectedCulture =
            this.selectedCulture === this._enCulture
                ? this._ukCulture
                : this._enCulture;
        window.localStorage.setItem("selectedCulture", this.selectedCulture);
        this._selectedDictionary = this._dictionary.items.find(
            (r) => r.culture == this.selectedCulture
        );
        this._serviceUtils.post("Translation/SetCulture", {
            culture: this.selectedCulture,
        });
    }
}
