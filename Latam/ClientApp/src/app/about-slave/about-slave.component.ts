import { Component, EventEmitter, HostListener, Output } from "@angular/core";

@Component({
    selector: "about-slave",
    templateUrl: "./about-slave.component.html",
    styleUrls: ["./about-slave.component.css"],
})
export class AboutSlaveComponent {
    @Output()
    public onSelectEmitter: EventEmitter<string> = new EventEmitter<string>();

    onSelect(path: string): void {
        this.onSelectEmitter.emit(path);
    }

    @HostListener("window:click", ["$event"])
    public onCLick(event: Event) {
        if (
            !event ||
            !event.target ||
            (event.target["id"] != "about-nav-item" &&
                event.target["id"] != "about-slave-container" &&
                (!event.target["parentElement"] ||
                    event.target["parentElement"]["id"] !=
                        "about-slave-container"))
        ) {
            this.onSelectEmitter.emit("");
        }
    }
}
