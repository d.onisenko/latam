import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Category } from "../model/product";
import { ServiceUtils } from "../utils/service-utils";

@Component({
    selector: "app-home",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.css"],
})
export class HomeComponent {
    public categories: Category[];

    constructor(private _serviceUtils: ServiceUtils, private _router: Router) {
        this._serviceUtils
            .get<Category[]>("Category/Get")
            .subscribe((response) => {
                this.categories = response;
            });
    }

    onClick(categoryName: string): void {
        let category = this.categories.find((c) => c.nameEn === categoryName);
        if (category) {
            let path = `shop/category?ids=(${category.id})`;
            this._router.navigateByUrl(`/${path}`);
        }
    }
}
