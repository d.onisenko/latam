import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Auth } from "../auth/auth.service";
import { User } from "../auth/user";
import { NgForm } from "@angular/forms";

@Component({
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.css"],
    selector: "login",
})
export class LoginComponent {
    public login: string = "";
    public password: string = "";
    public message: string = "";
    public requested: boolean = false;
    public isSuccess: boolean = false;

    constructor(
        private _authService: Auth,
        private _route: ActivatedRoute,
        private _router: Router
    ) {}

    public async authenticate(loginForm: NgForm) {
        if (!loginForm.valid || this.requested) {
            return;
        }
        this.requested = true;
        this._authService
            .login(this.login, this.password)
            .subscribe((result) => {
                this.afterLogin(result);
            });
    }

    private afterLogin(user: User) {
        this.requested = false;
        this.isSuccess = user !== null;
        if (!this.isSuccess) {
            this.message = "А вот и нет";
            return;
        }

        this._router.navigateByUrl(
            this._route.snapshot.queryParams["returnUrl"]
        );
    }
}
