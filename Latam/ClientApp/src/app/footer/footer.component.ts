import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Localizer } from "../localizer/localizer.service";
import { Category } from "../model/product";
import { ServiceUtils } from "../utils/service-utils";

@Component({
    selector: "latam-footer",
    templateUrl: "./footer.component.html",
    styleUrls: ["./footer.component.css"],
})
export class FooterComponent implements OnInit {
    public categories: Category[];

    constructor(
        private _localizer: Localizer,
        private _serviceUtils: ServiceUtils,
        private _router: Router
    ) {}

    ngOnInit(): void {
        if (this.categories) {
            return;
        }
        this._serviceUtils
            .get<Category[]>("Category/Get")
            .subscribe((response) => {
                this.categories = response;
            });
    }

    onSelect(path: string): void {
        this._router.navigateByUrl(`/${path}`);
    }

    onSelectCategory(categoryId: number): void {
        let selectedCategory = this.categories.find((c) => c.id === categoryId);
        if (selectedCategory) {
            let path = `shop/category?ids=(${selectedCategory.id})`;
            this._router.navigateByUrl(`/${path}`);
        }
    }
}
