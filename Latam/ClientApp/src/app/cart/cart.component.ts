import { Component, OnInit } from "@angular/core";
import { Contact } from "../model/contact";
import { Product } from "../model/product";
import { CartService } from "../utils/cart.service";
import { ServiceUtils } from "../utils/service-utils";
import { NgForm } from "@angular/forms";
import { Shop } from "../model/shop";
import { Order, ProductInOrder } from "../model/order";
import { BaseWebResponse } from "../model/baseWebResponse";
import { ProductDto } from "../model/product-dto";

@Component({
    selector: "cart",
    templateUrl: "./cart.component.html",
    styleUrls: ["./cart.component.css"],
})
export class CartComponent implements OnInit {
    public noProductsVisible: boolean = false;
    public messageSuccessVisible: boolean = false;
    public messageFailVisible: boolean = false;

    public products: Product[];
    public totalPrice: number;
    public isVisa: boolean = false;
    public isCash: boolean = true;
    public contact: Contact;
    public shops: Shop[];
    public selectedShopId: number;

    public isPickup: boolean = false;
    public isNpPickup: boolean = false;
    public isNpCourier: boolean = false;

    public npPickupCity: string = "";
    public npPickupNumber: string = "";

    public npCourierCity: string = "";
    public npCourierStreet: string = "";
    public npCourierHouse: string = "";
    public npCourierFlat: string = "";

    constructor(
        private _serviceUtils: ServiceUtils,
        private _cartService: CartService
    ) {
        this.contact = new Contact();
    }

    ngOnInit(): void {
        let productsFromCart = this._cartService.getProducts;
        if (!productsFromCart || !productsFromCart.length) {
            this.noProductsVisible = true;
            return;
        }
        this.noProductsVisible = false;

        this._serviceUtils
            .post<Product[]>(
                "Product/GetSeveralByIds",
                productsFromCart.map((p) => p.id)
            )
            .subscribe((response) => {
                response.forEach((p) => {
                    let inCart = productsFromCart.find((x) => x.id === p.id);
                    if (inCart) {
                        p.quantity = inCart.quantity;
                    }
                });

                this.products = response;
                this.onQuantityChanged();
            });

        this._serviceUtils.get<Shop[]>("Shop/Get").subscribe((response) => {
            this.shops = response;
        });
    }

    onPaymentChanged(value: string): void {
        if (value === "isVisa") {
            if (this.isVisa) {
                this.isCash = false;
            }
        } else if (value === "isCash") {
            if (this.isCash) {
                this.isVisa = false;
            }
        }
    }

    onDeliveryChanged(value: string): void {
        if (value === "isPickup") {
            if (this.isPickup) {
                this.isNpPickup = false;
                this.isNpCourier = false;
            }
        } else if (value === "isNpPickup") {
            if (this.isNpPickup) {
                this.isPickup = false;
                this.isNpCourier = false;
            }
        } else if (value === "isNpCourier") {
            if (this.isNpCourier) {
                this.isPickup = false;
                this.isNpPickup = false;
            }
        }
    }

    onClearCart(): void {
        this._cartService.removeAll();
        this.noProductsVisible = true;
    }

    onQuantityChanged(): void {
        this.totalPrice = parseFloat(
            this.products
                .map((p) => p.price * p.quantity)
                .reduce((a, b) => a + b)
                .toFixed(2)
        );

        this._cartService.removeAll();
        this.products
            .map((p) => new ProductDto(p.id, p.quantity))
            .forEach((pDto) => this._cartService.add(pDto));
    }

    onCreateOrder(
        contact: NgForm,
        npPickupForm: NgForm,
        npCourierForm: NgForm
    ): void {
        if (
            !contact.valid ||
            (this.isNpPickup && !npPickupForm.valid) ||
            (this.isNpCourier && !npCourierForm.valid)
        ) {
            return;
        }

        let productsInOrder = [];
        this.products.forEach((p) => {
            let productInOrder = new ProductInOrder();
            productInOrder.productId = p.id;
            productInOrder.quantity = p.quantity;
            productInOrder.price = p.quantity * p.price;
            productsInOrder.push(productInOrder);
            productInOrder.product = p;
        });

        let order = new Order();
        order.totalPrice = this.totalPrice;
        order.productInOrder = productsInOrder;
        order.isVisa = this.isVisa;
        order.isCash = this.isCash;
        order.contactName = this.contact.name;
        order.contactSurname = this.contact.surname;
        order.contactPhone = this.contact.phone;
        order.contactEmail = this.contact.email;
        order.comment = this.contact.comment;

        if (this.isPickup) {
            order.isPickup = true;
            order.pickupShopId = this.selectedShopId;
        } else if (this.isNpPickup) {
            order.isNpPickup = true;
            order.city = this.npPickupCity;
            order.npPickupNumber = this.npPickupNumber;
        } else if (this.isNpCourier) {
            order.isNpCourier = true;
            order.city = this.npCourierCity;
            order.street = this.npCourierStreet;
            order.house = this.npCourierHouse;
            order.flat = this.npCourierFlat;
        }

        if (!order.isPickup && !order.isNpPickup && !order.isNpCourier) {
            return;
        }

        this._serviceUtils
            .post<BaseWebResponse>("Order/Create", order)
            .subscribe((response) => {
                if (!response.isSuccess) {
                    console.log(response.message);
                    this.messageFailVisible = true;
                } else {
                    this._cartService.removeAll();
                    this.messageSuccessVisible = true;
                }
            });
    }
}
