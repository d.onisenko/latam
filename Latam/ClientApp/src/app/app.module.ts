import { BrowserModule } from "@angular/platform-browser";
import { APP_INITIALIZER, NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { RouterModule, Routes } from "@angular/router";

import { AppComponent } from "./app.component";
import { NavMenuComponent } from "./nav-menu/nav-menu.component";
import { HomeComponent } from "./home/home.component";
import { LocalizePipe } from "./localizer/localizer.pipe";
import { Localizer } from "./localizer/localizer.service";
import { ShopComponent } from "./shop/shop-page/shop.component";
import { FilterShopComponent } from "./shop/filter-shop/filter-shop.component";
import { SortShopComponent } from "./shop/sort-shop/sort-shop.component";
import { ProductComponent } from "./product/product-page.component";
import { FooterComponent } from "./footer/footer.component";
import { AboutComponent } from "./about/about.component";
import { LocationComponent } from "./location/location.component";
import { ContactComponent } from "./contact/contact.component";
import { AboutSlaveComponent } from "./about-slave/about-slave.component";
import { ShopSlaveComponent } from "./shop-slave/shop-slave.component";
import { SearchComponent } from "./search/search-input/search.component";
import { SearchSlaveComponent } from "./search/search-slave/search-slave.component";
import { CartComponent } from "./cart/cart.component";
import { OrdersComponent } from "./orders/orders.component";
import { Auth } from "./auth/auth.service";
import { BasicAuthInterceptor } from "./auth/basic-auth-interceptor";
import { LoginComponent } from "./login/login.component";
import { AuthGuard } from "./auth/auth.guard";
import { EditSlaveComponent } from "./edit/edit-slave/edit-slave.component";
import { EditShopComponent } from "./edit/edit-shop/edit-shop.component";
import { EditProductComponent } from "./edit/edit-product/edit-product.component";
import { DeliveryAndPaymentComponent } from "./delivery-and-payment/delivery-and-payment.component";
import { NavMenuSlaveComponent } from "./nav-menu/nav-menu-slave/nav-menu-slave.component";

export function localizerProvider(provider: Localizer) {
    return async () => {
        await provider.init();
    };
}

export function getLastProductId() {
    return window.sessionStorage.getItem("productId");
}

const routes: Routes = [
    {
        path: "shop",
        component: ShopComponent,
        pathMatch: "full",
    },
    {
        path: "edit-shop",
        component: EditShopComponent,
        pathMatch: "full",
        canActivate: [AuthGuard],
    },
    {
        path: "edit-product",
        component: EditProductComponent,
        pathMatch: "full",
        canActivate: [AuthGuard],
    },
    { path: "login", component: LoginComponent, pathMatch: "full" },
    { path: "shop/category", component: ShopComponent, pathMatch: "full" },
    { path: "shop/category/:id", component: ShopComponent },
    { path: "contact", component: ContactComponent, pathMatch: "full" },
    { path: "location", component: LocationComponent, pathMatch: "full" },
    { path: "about", component: AboutComponent, pathMatch: "full" },
    {
        path: "delivery-and-payment",
        component: DeliveryAndPaymentComponent,
        pathMatch: "full",
    },
    { path: "cart", component: CartComponent, pathMatch: "full" },
    {
        path: "orders",
        component: OrdersComponent,
        pathMatch: "full",
        canActivate: [AuthGuard],
    },
    { path: "", component: HomeComponent },
    { path: "product", redirectTo: `product/?id=${getLastProductId()}` },
    {
        path: "product/:id",
        component: ProductComponent,
    },
    { path: "**", redirectTo: "" },
];

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,
        ShopComponent,
        FilterShopComponent,
        SortShopComponent,
        ProductComponent,
        LocalizePipe,
        FooterComponent,
        AboutComponent,
        LocationComponent,
        ContactComponent,
        AboutSlaveComponent,
        ShopSlaveComponent,
        SearchComponent,
        SearchSlaveComponent,
        CartComponent,
        OrdersComponent,
        LoginComponent,
        EditSlaveComponent,
        EditShopComponent,
        EditProductComponent,
        DeliveryAndPaymentComponent,
        NavMenuSlaveComponent,
    ],
    imports: [
        BrowserModule.withServerTransition({ appId: "ng-cli-universal" }),
        HttpClientModule,
        FormsModule,
        RouterModule.forRoot(routes, {
            relativeLinkResolution: "legacy",
            scrollPositionRestoration: "top",
        }),
    ],
    providers: [
        LocalizePipe,
        Localizer,
        {
            provide: APP_INITIALIZER,
            useFactory: localizerProvider,
            deps: [Localizer],
            multi: true,
        },
        Auth,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: BasicAuthInterceptor,
            multi: true,
        },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
