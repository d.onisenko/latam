import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { SearchDto } from "src/app/model/search-dto";
import { ServiceUtils } from "src/app/utils/service-utils";

@Component({
    selector: "search",
    templateUrl: "./search.component.html",
    styleUrls: ["./search.component.css"],
})
export class SearchComponent {
    public searchValue: string = "";
    public searchInputVisible: boolean = false;
    public searchSlaveVisible: boolean = false;

    public searchResults: SearchDto[];
    public searchResultsSections: string[];

    constructor(private _serviceUtils: ServiceUtils, private _router: Router) {}

    onSearchImgClick(): void {
        this.searchInputVisible = !this.searchInputVisible;
    }

    onSearch(): void {
        if (!this.searchValue) {
            return;
        }
        this._serviceUtils
            .get<SearchDto[]>(`Search/Execute/?searchValue=${this.searchValue}`)
            .subscribe((response) => {
                this.searchResultsSections = [
                    ...new Set(response.map((x) => x.sectionName)),
                ];
                this.searchResults = response;
                if (this.searchResults) {
                    this.searchSlaveVisible = true;
                }
            });
    }

    onClick(path: string): void {
        this.searchSlaveVisible = false;
        if (path) {
            this._router.navigateByUrl(`/${path}`);
        }
    }
}
