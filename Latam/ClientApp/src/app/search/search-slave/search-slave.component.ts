import {
    Component,
    EventEmitter,
    HostListener,
    Input,
    Output,
} from "@angular/core";
import { SearchDto } from "src/app/model/search-dto";

@Component({
    selector: "search-slave",
    templateUrl: "./search-slave.component.html",
    styleUrls: ["./search-slave.component.css"],
})
export class SearchSlaveComponent {
    @Input()
    public results: SearchDto[];
    @Input()
    public resultsSections: string[];
    @Output()
    public clickEmitter: EventEmitter<string> = new EventEmitter<string>();

    @HostListener("window:click", ["$event"])
    public onCLick(event: Event) {
        let classList = event && event.target && event.target["classList"];
        if (
            !event ||
            !event.target ||
            !classList ||
            ![
                "result-items-container",
                "search-slave",
                "section-container",
                "section-label",
            ].includes(classList[0])
        ) {
            this.clickEmitter.emit("");
        }
    }

    public onSelect(dto: SearchDto): void {
        let path = "";
        if (dto.sectionName === "Category") {
            path = `shop/category?ids=(${dto.id})`;
        } else if (dto.sectionName === "Product") {
            path = `product/?id=${dto.id}`;
        }
        this.clickEmitter.emit(path);
    }
}
