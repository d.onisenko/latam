import { Component, OnChanges, OnInit, SimpleChanges } from "@angular/core";
import { DataColumn, Order, OrderStatus, OrderViewModel } from "../model/order";
import { ServiceUtils } from "../utils/service-utils";

@Component({
    selector: "orders",
    templateUrl: "./orders.component.html",
    styleUrls: ["./orders.component.css"],
})
export class OrdersComponent implements OnInit {
    public orders: Order[];
    public orderViewModels: OrderViewModel[];
    public columns: DataColumn[];
    public selected: OrderViewModel;
    public selectedId: number;
    public needSave: boolean = false;
    public visitorCount: number = 0;

    public statuses: OrderStatus[];

    constructor(private _serviceUtils: ServiceUtils) {}

    ngOnInit(): void {
        this._serviceUtils
            .get<OrderStatus[]>("Order/GetStatuses")
            .subscribe((response) => {
                this.statuses = response;
            });

        this._serviceUtils.get<Order[]>("Order/Get").subscribe((response) => {
            this.orders = response;
            if (this.orders.length) {
                this.orderViewModels = [];
                this.orders.forEach((order) => {
                    this.orderViewModels.push(new OrderViewModel(order));
                });
                this.columns = this.orderViewModels[0].dataColumns;
            }
        });

        this._serviceUtils
            .get<number>("Order/GetVisitorCount")
            .subscribe((response) => {
                this.visitorCount = response;
            });
    }

    onOrderChanged(orderViewModel: OrderViewModel): void {
        orderViewModel.changed = true;
        this.needSave = true;
    }

    onRowSelected(orderViewModel: OrderViewModel): void {
        if (this.selectedId === orderViewModel.order.id) {
            this.selectedId = null;
            this.selected = null;
        } else {
            this.selected = orderViewModel;
            this.selectedId = orderViewModel.order.id;
        }
    }

    onSave(): void {
        let ordersToSave = this.orderViewModels
            .filter((o) => o.changed)
            .map((o) => o.order);
        if (!ordersToSave.length) {
            return;
        }
        this._serviceUtils.post("Order/Save", ordersToSave).subscribe(() => {
            this.selectedId = null;
            this.selected = null;
            this.needSave = false;
            this.ngOnInit();
        });
    }

    getStatusClass(column: DataColumn): string {
        if (column.name !== "Статус") {
            return "";
        }
        if (column.value === "Новий") {
            return "new-status";
        }
        if (column.value === "Сплачений") {
            return "paid-status";
        }
        if (column.value === "Надісланий") {
            return "sent-status";
        }
    }

    trackByIndex(index: number) {
        return index;
    }
}
