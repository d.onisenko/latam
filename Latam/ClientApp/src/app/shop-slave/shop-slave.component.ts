import {
    Component,
    EventEmitter,
    HostListener,
    Input,
    Output,
} from "@angular/core";
import { Category } from "../model/product";

@Component({
    selector: "shop-slave",
    templateUrl: "./shop-slave.component.html",
    styleUrls: ["./shop-slave.component.css"],
})
export class ShopSlaveComponent {
    @Input()
    public categories: Category[];

    @Output()
    public onSelectEmitter: EventEmitter<string> = new EventEmitter<string>();

    onSelect(categoryId: number): void {
        let selectedCategory = this.categories.find((c) => c.id === categoryId);
        if (selectedCategory) {
            let path = `shop/category?ids=(${selectedCategory.id})`;
            this.onSelectEmitter.emit(path);
        }
    }

    @HostListener("window:click", ["$event"])
    public onCLick(event: Event) {
        if (
            !event ||
            !event.target ||
            (event.target["id"] != "shop-nav-item" &&
                event.target["id"] != "shop-slave-container" &&
                (!event.target["parentElement"] ||
                    event.target["parentElement"]["id"] !=
                        "shop-slave-container"))
        ) {
            this.onSelectEmitter.emit("");
        }
    }
}
