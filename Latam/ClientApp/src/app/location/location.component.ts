import { Component, OnInit } from "@angular/core";
import { Shop } from "../model/shop";
import { ServiceUtils } from "../utils/service-utils";

@Component({
    selector: "location",
    templateUrl: "./location.component.html",
    styleUrls: ["./location.component.css"],
})
export class LocationComponent implements OnInit {
    public shops: Shop[];

    constructor(private _serviceUtils: ServiceUtils) {}

    ngOnInit(): void {
        this._serviceUtils.get<Shop[]>("Shop/Get").subscribe((response) => {
            this.shops = response;
        });
    }
}
