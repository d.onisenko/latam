import { Component, EventEmitter, Output } from "@angular/core";

@Component({
    selector: "sort-shop",
    templateUrl: "./sort-shop.component.html",
    styleUrls: ["./sort-shop.component.css"],
})
export class SortShopComponent {
    public byNameDesc: boolean = false;
    public byNameAsc: boolean = false;
    public byPriceDesc: boolean = false;
    public byPriceAsc: boolean = false;
    public byDateAsc: boolean = false;
    public byDateDesc: boolean = false;

    public sortValue: string;

    @Output()
    public sortEmitter: EventEmitter<string> = new EventEmitter<string>();

    onSort(target: string) {
        let properties = Object.getOwnPropertyNames(this).filter((s) =>
            s.startsWith("by")
        );
        properties.forEach((p) => {
            this[p] = false;
        });
        if (this.sortValue && this.sortValue.startsWith(target)) {
            this.sortValue.includes("Asc")
                ? (this.sortValue = target + "Desc")
                : (this.sortValue = target + "Asc");
        } else {
            this.sortValue = target + "Asc";
        }
        this[this.sortValue] = true;
        this.sortEmitter.emit(this.sortValue);
    }
}
