import {
    AfterContentChecked,
    Component,
    EventEmitter,
    Input,
    Output,
} from "@angular/core";
import { Router } from "@angular/router";
import { FilterItem } from "src/app/model/filter-item";
import { Category, Purpose } from "src/app/model/product";

@Component({
    selector: "filter-shop",
    templateUrl: "./filter-shop.component.html",
    styleUrls: ["./filter-shop.component.css"],
})
export class FilterShopComponent implements AfterContentChecked {
    @Input()
    public categories: Category[];

    @Input()
    public purposes: Purpose[];

    public selectedCategories: number[] = null;
    public selectedPurposes: number[] = null;

    public isCollapsed: boolean = true;

    @Output()
    public filterEmitter: EventEmitter<FilterItem> = new EventEmitter<FilterItem>();

    constructor(private _router: Router) {}

    ngAfterContentChecked(): void {
        if (!this.selectedPurposes && this.purposes) {
            this.selectedPurposes = this.purposes.map((c) => c.id);
        }
        if (this.categories) {
            let selectedFromRoute = this.categories
                .filter((x) => x.isSelected)
                .map((c) => c.id);
            if (selectedFromRoute && selectedFromRoute.length) {
                this.selectedCategories = selectedFromRoute;
            } else {
                this.selectedCategories = this.categories.map((c) => c.id);
            }
        }
    }

    public onCollapseClick() {
        this.isCollapsed = !this.isCollapsed;
    }

    onFilter() {
        let filterItem = new FilterItem();
        filterItem.categoriesId = this.selectedCategories;
        filterItem.purposesId = this.selectedPurposes;

        this.filterEmitter.emit(filterItem);
    }

    onCategoryClick(category: Category): void {
        let id = category.id;
        if (this.selectedCategories.includes(id)) {
            this.selectedCategories = this.selectedCategories.filter(
                (c) => c !== id
            );
        } else {
            this.selectedCategories.push(id);
        }
        this._router.navigateByUrl(
            `shop/category?ids=(${this.selectedCategories.toString()})`
        );
    }

    onPurposeClick(purpose: Purpose): void {
        let id = purpose.id;
        if (this.selectedPurposes.includes(id)) {
            this.selectedPurposes = this.selectedPurposes.filter(
                (c) => c !== id
            );
        } else {
            this.selectedPurposes.push(id);
        }
        this.onFilter();
    }
}
