import { AfterViewChecked, Component, OnInit, ViewChild } from "@angular/core";
import { FilterShopComponent } from "../filter-shop/filter-shop.component";
import { FilterItem } from "../../model/filter-item";
import { Category, Purpose, Product } from "../../model/product";
import { SortShopComponent } from "../sort-shop/sort-shop.component";
import { ServiceUtils } from "../../utils/service-utils";
import { ActivatedRoute, Router } from "@angular/router";
import { ProductDto } from "src/app/model/product-dto";
import { CartService } from "src/app/utils/cart.service";

@Component({
    selector: "shop",
    templateUrl: "./shop.component.html",
    styleUrls: ["./shop.component.css"],
})
export class ShopComponent implements OnInit, AfterViewChecked {
    public products: Product[];
    public filteredProducts: Product[];
    public categories: Category[];
    public categoriesIdFromRoute: number[] = [];
    public purposes: Purpose[];

    constructor(
        private _serviceUtils: ServiceUtils,
        private _router: Router,
        private _route: ActivatedRoute,
        private _cartService: CartService
    ) {}

    @ViewChild(FilterShopComponent, { static: true })
    public filterShop: FilterShopComponent;

    @ViewChild(SortShopComponent, { static: true })
    public sortShop: SortShopComponent;

    ngAfterViewChecked(): void {
        this._route.queryParams.subscribe((queryParams) => {
            let ids = queryParams["ids"];
            if (ids) {
                let parsedIds = ids
                    .replace("(", "")
                    .replace(")", "")
                    .split(",")
                    .map((x) => parseInt(x))
                    .filter((x) => x);

                let equals = true;
                if (parsedIds.length != this.categoriesIdFromRoute.length) {
                    equals = false;
                }
                if (
                    equals &&
                    parsedIds.find(
                        (p) => !this.categoriesIdFromRoute.includes(p)
                    )
                ) {
                    equals = false;
                }

                if (!equals) {
                    this.categoriesIdFromRoute = parsedIds;
                    this.ngOnInit();
                }
            }
        });
    }

    ngOnInit(): void {
        this._serviceUtils
            .get<Product[]>("Product/Get")
            .subscribe((response) => {
                this.products = response;

                this.products.forEach((p) => {
                    p.descriptionEn = p.descriptionEn.split(".")[0] + ".";
                    p.descriptionUk = p.descriptionUk.split(".")[0] + ".";
                });

                this.filteredProducts = response;

                this.categories = [
                    ...new Map(
                        response.map((r) => [r.category.id, r.category])
                    ).values(),
                ];
                this.categories.forEach((category) => {
                    category.isSelected = this.categoriesIdFromRoute.includes(
                        category.id
                    );
                });
                this.filterShop.categories = this.categories;
                this.purposes = [];
                response.forEach((product) => {
                    let purposes = product.productInPurpose.map(
                        (x) => x.purpose
                    );
                    purposes.forEach((purpose) => {
                        if (!this.purposes.find((p) => p.id === purpose.id)) {
                            this.purposes.push(purpose);
                        }
                    });
                });

                if (this.categoriesIdFromRoute.length) {
                    this.filteredProducts = this.products.filter((p) =>
                        this.categoriesIdFromRoute.includes(p.categoryId)
                    );
                }

                this.setPictures();
            });
    }

    setPictures() {
        this.products.forEach((p) => {
            p.pictureUrl = [];
            let picName = p.nameEn.toLowerCase().replace(/\s/g, "");
            p.pictureUrl.push(`assets\\production\\${picName}_1.jpg`);
        });
    }

    onFilter(filterItem: FilterItem): void {
        let categories = filterItem.categoriesId;
        let purposes = filterItem.purposesId;

        this.filteredProducts = this.products.filter(
            (p) =>
                categories.includes(p.categoryId) &&
                purposes.filter((x) =>
                    p.productInPurpose.map((p) => p.purposeId).includes(x)
                ).length
        );
    }

    onSort(sortValue: string): void {
        let byNameAsc = sortValue.includes("byNameAsc");
        let byNameDesc = sortValue.includes("byNameDesc");
        let byPriceAsc = sortValue.includes("byPriceAsc");
        let byPriceDesc = sortValue.includes("byPriceDesc");
        let byDateAsc = sortValue.includes("byDateAsc");
        let byDateDesc = sortValue.includes("byDateDesc");

        this.filteredProducts = this.filteredProducts.sort((p1, p2) => {
            if (
                (byNameAsc && p1.nameEn > p2.nameEn) ||
                (byNameDesc && p1.nameEn < p2.nameEn) ||
                (byPriceAsc && p1.price > p2.price) ||
                (byPriceDesc && p1.price < p2.price) ||
                (byDateAsc && p1.createdOn > p2.createdOn) ||
                (byDateDesc && p1.createdOn < p2.createdOn)
            ) {
                return 1;
            }

            if (
                (byNameAsc && p1.nameEn < p2.nameEn) ||
                (byNameDesc && p1.nameEn > p2.nameEn) ||
                (byPriceAsc && p1.price < p2.price) ||
                (byPriceDesc && p1.price > p2.price) ||
                (byDateAsc && p1.createdOn < p2.createdOn) ||
                (byDateDesc && p1.createdOn > p2.createdOn)
            ) {
                return -1;
            }

            return 0;
        });
    }

    onClick(product: Product) {
        this._router.navigateByUrl(`product/?id=${product.id}`);
    }

    onAddToCart(id: number) {
        let productDto = new ProductDto(id, 1);
        this._cartService.add(productDto);
    }
}
