import { Component } from "@angular/core";

@Component({
    selector: "delivery-and-payment",
    templateUrl: "./delivery-and-payment.component.html",
    styleUrls: ["./delivery-and-payment.component.css"],
})
export class DeliveryAndPaymentComponent {}
