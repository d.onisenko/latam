import { Inject, Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { map, retry, tap } from "rxjs/operators";

@Injectable({
    providedIn: "root",
})
export class ServiceUtils {
    constructor(
        private _http: HttpClient,
        @Inject("BASE_URL") private _baseUrl: string
    ) {}

    private getOptions(): object {
        let headers = new HttpHeaders({
            "Content-Type": "application/json; charset=utf-8",
        });
        return { headers };
    }

    create<T>(c: { new (): T }): T {
        return new c();
    }

    public get<T>(path: string): Observable<T> {
        return this._http.get(this._baseUrl + path, this.getOptions()).pipe(
            retry(3),
            map((result) => result as T)
        );
    }

    public post<T>(path: string, data?: any): Observable<T> {
        return this._http
            .post<any>(this._baseUrl + path, data, this.getOptions())
            .pipe(
                retry(3),
                map((result) => result as T)
            );
    }
}
