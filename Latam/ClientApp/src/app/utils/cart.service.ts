import { Injectable, OnDestroy } from "@angular/core";
import { ProductDto } from "../model/product-dto";
import { CartSubscriber } from "../model/subscriber";

@Injectable({
    providedIn: "root",
})
export class CartService implements OnDestroy {
    protected subscribers: CartSubscriber[] = [];

    public subscribeOnChanges(scope: object, handler: Function) {
        this.subscribers.push(new CartSubscriber(scope, handler));
    }

    public add(product: ProductDto): void {
        let products = this.getProducts;
        let existed = products.find((p) => p.id === product.id);
        if (existed) {
            existed.quantity += product.quantity;
        } else {
            products.push(product);
        }
        window.localStorage.setItem("cart", JSON.stringify(products));
        this.handle();
    }

    public remove(id: number): void {
        let products = this.getProducts;
        products = products.filter((p) => p.id !== id);
        window.localStorage.setItem("cart", JSON.stringify(products));
        this.handle();
    }

    public removeAll(): void {
        window.localStorage.setItem("cart", null);
        this.handle();
    }

    public get count(): number {
        let products = this.getProducts;
        return products && products.length
            ? this.getProducts.map((p) => p.quantity).reduce((a, b) => a + b)
            : 0;
    }

    public get getProducts(): ProductDto[] {
        let cart = window.localStorage.getItem("cart");
        let products = <ProductDto[]>JSON.parse(cart);
        return products || <ProductDto[]>[];
    }

    private handle() {
        this.subscribers.forEach((subscriber) => {
            subscriber.handler.call(subscriber.scope);
        });
    }

    ngOnDestroy(): void {
        this.subscribers = [];
    }
}
