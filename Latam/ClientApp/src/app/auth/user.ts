export class User {
  id: number;
  login: string;
  authdata?: string;
  isAdmin: boolean;
}
