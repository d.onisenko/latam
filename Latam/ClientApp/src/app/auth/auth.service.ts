import { HttpClient } from "@angular/common/http";
import { Injectable, Injector } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";
import { User } from "./user";

@Injectable()
export class Auth {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private _injector: Injector) {
        this.currentUserSubject = new BehaviorSubject<User>(
            JSON.parse(localStorage.getItem("currentUser"))
        );
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return (
            (this.currentUserSubject && this.currentUserSubject.value) || null
        );
    }

    login(username: string, password: string) {
        return this._injector
            .get(HttpClient)
            .post<any>(`Auth/Login`, {
                username,
                password,
            })
            .pipe(
                map((user) => {
                    return this.afterLogin(username, password, user);
                })
            );
    }

    private afterLogin(username: string, password: string, user: User): User {
        if (user) {
            user.authdata = window.btoa(username + ":" + password);
            localStorage.setItem("currentUser", JSON.stringify(user));
            this.currentUserSubject.next(user);
        } else {
            this.logout();
        }
        return user;
    }

    logout() {
        localStorage.removeItem("currentUser");
        this.currentUserSubject.next(null);
    }
}
