import { Injectable } from "@angular/core";
import {
    Router,
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
} from "@angular/router";

import { Auth } from "./auth.service";

@Injectable({ providedIn: "root" })
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private authenticationService: Auth) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authenticationService.currentUserValue;
        if (currentUser) {
            return true;
        }

        this.router.navigate(["/login"], {
            queryParams: { returnUrl: state.url },
        });
        return false;
    }

    isLoggedIn(): boolean {
        const currentUser = this.authenticationService.currentUserValue;
        return currentUser ? true : false;
    }

    isAdmin(): boolean {
        const currentUser = this.authenticationService.currentUserValue;
        return currentUser && currentUser.isAdmin;
    }
}
