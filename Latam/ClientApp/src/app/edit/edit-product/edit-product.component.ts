import { Component, OnInit } from "@angular/core";
import { BaseWebResponse } from "src/app/model/baseWebResponse";
import { Product } from "src/app/model/product";
import { ServiceUtils } from "src/app/utils/service-utils";

@Component({
    selector: "edit-product",
    templateUrl: "./edit-product.component.html",
    styleUrls: ["./edit-product.component.css"],
})
export class EditProductComponent implements OnInit {
    public products: Product[];

    constructor(private _serviceUtils: ServiceUtils) {}

    ngOnInit(): void {
        this._serviceUtils
            .get<Product[]>("Product/Get")
            .subscribe((response) => {
                this.products = response;
            });
    }

    onSave(): void {
        let changed = this.products.filter((x) => x.changed);
        if (!changed.length) {
            return;
        }
        this._serviceUtils
            .post<BaseWebResponse>("Product/Edit", changed)
            .subscribe((response) => {
                if (response.isSuccess) {
                    this.ngOnInit();
                } else {
                    console.log(response.message);
                }
            });
    }

    onModelChanged(product: Product): void {
        let selectedProduct = this.products.find((x) => x.id === product.id);
        if (selectedProduct) {
            selectedProduct.changed = true;
        }
    }
}
