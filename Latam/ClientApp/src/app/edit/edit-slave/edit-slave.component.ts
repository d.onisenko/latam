import { Component, EventEmitter, HostListener, Output } from "@angular/core";

@Component({
    selector: "edit-slave",
    templateUrl: "./edit-slave.component.html",
    styleUrls: ["./edit-slave.component.css"],
})
export class EditSlaveComponent {
    @Output()
    public onSelectEmitter: EventEmitter<string> = new EventEmitter<string>();

    onSelect(path: string): void {
        this.onSelectEmitter.emit(path);
    }

    @HostListener("window:click", ["$event"])
    public onCLick(event: Event) {
        if (
            !event ||
            !event.target ||
            (event.target["id"] != "edit-nav-item" &&
                event.target["id"] != "edit-slave-container" &&
                (!event.target["parentElement"] ||
                    event.target["parentElement"]["id"] !=
                        "edit-slave-container"))
        ) {
            this.onSelectEmitter.emit("");
        }
    }
}
