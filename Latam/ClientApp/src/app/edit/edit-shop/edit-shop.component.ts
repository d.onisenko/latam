import { Component, OnInit } from "@angular/core";
import { BaseWebResponse } from "src/app/model/baseWebResponse";
import { Shop } from "src/app/model/shop";
import { ServiceUtils } from "src/app/utils/service-utils";

@Component({
    selector: "edit-shop",
    templateUrl: "./edit-shop.component.html",
    styleUrls: ["./edit-shop.component.css"],
})
export class EditShopComponent implements OnInit {
    public shops: Shop[];

    constructor(private _serviceUtils: ServiceUtils) {}

    ngOnInit(): void {
        this._serviceUtils.get<Shop[]>("Shop/Get").subscribe((response) => {
            this.shops = response;
        });
    }

    onSave(): void {
        let changed = this.shops.filter((x) => x.changed);
        let toRemove = this.shops.filter((x) => x.toRemove);
        if (!changed.length && !toRemove.length) {
            return;
        }
        this._serviceUtils
            .post<BaseWebResponse>("Shop/Edit", changed)
            .subscribe((response) => {
                if (response.isSuccess) {
                    this.removeSelected(toRemove);
                } else {
                    console.log(response.message);
                }
            });
    }

    removeSelected(toRemove: Shop[]): void {
        if (!toRemove || !toRemove.length) {
            this.ngOnInit();
            return;
        }
        this._serviceUtils
            .post<BaseWebResponse>("Shop/Remove", toRemove)
            .subscribe((response) => {
                if (response.isSuccess) {
                    this.ngOnInit();
                } else {
                    console.log(response.message);
                }
            });
    }

    onRemove(shop: Shop): void {
        let selectedShop = this.shops.find((x) => x.id === shop.id);
        if (selectedShop) {
            selectedShop.toRemove = true;
        }
    }

    onAdd(): void {
        let newShop = new Shop();
        this.shops.unshift(newShop);
    }

    onModelChanged(shop: Shop): void {
        let selectedShop = this.shops.find((x) => x.id === shop.id);
        if (selectedShop) {
            selectedShop.changed = true;
        }
    }
}
