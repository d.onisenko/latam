﻿using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Latam.Model.Utils {
    public static class ConvertUtils {
        public static object GetDefault(Type type) {
            if (type.IsValueType)
                return Activator.CreateInstance(type);

            return null;
        }

        public static T GetDefault<T>() {
            return (T)GetDefault(typeof(T));
        }

        public static T GetSafe<T>(Func<T> func) {
            try {
                return func();
            }
            catch {
                return GetDefault<T>();
            }
        }

        public static bool IsNullable(this Type type) {
            return !type.IsValueType || type.IsGenericType(typeof(Nullable<>));
        }

        public static bool IsGenericType(this Type type, Type genericType) {
            var types = type.GetInterfaces().Concat(new[] { type });
            return types.Any(x => (x.IsGenericType && x.GetGenericTypeDefinition() == genericType) || x == genericType);
        }

        public static bool TryConvert(object value, Type type, out object convertedValue) {
            convertedValue = GetDefault(type);
            try {
                if (value == null)
                    return true;

                if (value.GetType() == type) {
                    convertedValue = value;
                    return true;
                }

                if (type.IsEnum) {
                    var enumItem = value.ToString();
                    if (Enum.GetNames(type).Contains(enumItem)) {
                        convertedValue = Enum.Parse(type, value.ToString());
                        return true;
                    }
                }
                else if (value is string) {
                    var tryParseMethod = type.GetMethod("TryParse",
                        BindingFlags.Static | BindingFlags.Public,
                        null,
                        new[] { typeof(string), type.MakeByRefType() },
                        null);

                    if (tryParseMethod != null) {
                        var args = new[] { value, convertedValue };
                        var tryParseResult = (bool)tryParseMethod.Invoke(null, args);
                        convertedValue = args[1];
                        return tryParseResult;
                    }
                }
            }
            catch {
                return false;
            }
            return false;
        }

        public static bool IsNullOrDbNull(object value) {
            return value == null || DBNull.Value.Equals(value);
        }

        public static TResult ValueAsType<TResult>(object value) {
            return (TResult)ValueAsType(typeof(TResult), value);
        }

        public static object ValueAsType(Type type, object value) {
            if (value == null)
                return GetDefault(type);

            if (IsNullOrDbNull(value)) {
                return type == typeof(string)
                    ? string.Empty
                    : GetDefault(type);
            }

            var srcType = value.GetType();
            var dstType = type;
            if (srcType == dstType)
                return value;

            if (dstType.IsNullable()) {
                dstType = dstType.GenericTypeArguments.Length == 0
                    ? dstType
                    : dstType.GenericTypeArguments[0];
            }

            // TODO: грязный хак.
            if (value is IList && dstType.IsArray)
                return ((IList)value).Cast<string>().ToArray();

            else if (dstType.IsAssignableFrom(srcType))
                return value;

            else if (dstType.IsEnum)
                return Enum.Parse(dstType, value.ToString());

            else if (TryConvert(value, dstType, out var tempValue))
                return tempValue;

            else if (srcType == typeof(string) && dstType == typeof(byte[]))
                return Convert.FromBase64String((string)value);

            else if (value is IConvertible)
                return Convert.ChangeType(value, dstType, CultureInfo.InvariantCulture);

            return Convert.ChangeType(value.ToString(), dstType, CultureInfo.InvariantCulture);
        }
    }
}
