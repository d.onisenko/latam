﻿using System.Collections.Generic;
using System.Linq;

namespace Latam.Model.Utils {
    public static class AuthExtensions {
        public static IEnumerable<AuthUser> WithoutPasswords(this IEnumerable<AuthUser> users) {
            return users.Select(x => x.WithoutPassword());
        }

        public static AuthUser WithoutPassword(this AuthUser user) {
            user.Password = null;
            return user;
        }
    }
}
