﻿using Latam.Model.Dto;
using Latam.Model.Entities;
using Latam.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Latam.Model {
    public class ProductRepository {

        private readonly IUnitOfWork _unitOfWork;

        public ProductRepository(IUnitOfWork unitOfWork) {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Product> Get() => _unitOfWork.Product
            .AsNoTracking()
            .Include(p => p.Category)
            .Include(p => p.ProductInPurpose)
            .ThenInclude(p => p.Purpose);

        public Product GetById(int id) => _unitOfWork.Product
            .AsNoTracking()
            .Include(p => p.Category)
            .Include(p => p.ProductInPurpose)
            .FirstOrDefault(p => p.Id == id);

        public IEnumerable<Product> GetSeveralByIds(IEnumerable<int> ids) =>
            _unitOfWork.Product
                .AsNoTracking()
                .Where(p => ids.Contains(p.Id));

        public BaseWebResponse Edit(IEnumerable<Product> products) {
            var response = new BaseWebResponse();
            try {
                foreach (var product in products) {
                    var existed = _unitOfWork.Product
                        .AsNoTracking()
                        .FirstOrDefault(x => x.Id == product.Id);
                    if (existed != null) {
                        _unitOfWork.Product.Update(product);
                    }
                }
                response.IsSuccess = true;
            }
            catch (Exception ex) {
                response.Message = ex.InnerException?.ToString() ?? ex.Message;
            }
            return response;
        }
    }
}
