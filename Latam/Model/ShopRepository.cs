﻿using Latam.Model.Dto;
using Latam.Model.Entities;
using Latam.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Latam.Model {
    public class ShopRepository {
        private readonly IUnitOfWork _unitOfWork;
        public ShopRepository(IUnitOfWork unitOfWork) {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Shop> Get() => _unitOfWork.Shop
            .AsNoTracking()
            .AsEnumerable();

        public BaseWebResponse Edit(IEnumerable<Shop> shops) {
            var response = new BaseWebResponse();
            try {
                foreach (var shop in shops) {
                    var existed = _unitOfWork.Shop
                        .AsNoTracking()
                        .FirstOrDefault(x => x.Id == shop.Id);
                    if (existed != null) {
                        _unitOfWork.Shop.Update(shop);
                    } else if (!string.IsNullOrEmpty(shop.NameEn) && !string.IsNullOrEmpty(shop.NameUk)) {
                        _unitOfWork.Shop.Add(shop);
                    }
                    
                }
                response.IsSuccess = true;
            }
            catch (Exception ex) {
                response.Message = ex.InnerException?.ToString() ?? ex.Message;
            }
            return response;
        }

        public BaseWebResponse Remove(IEnumerable<Shop> shops) {
            var response = new BaseWebResponse();
            try {
                foreach (var shop in shops) {
                    var existed = _unitOfWork.Shop
                        .AsNoTracking()
                        .FirstOrDefault(x => x.Id == shop.Id);
                    if (existed != null) {
                        _unitOfWork.Shop.Delete(shop);
                    }
                }
                response.IsSuccess = true;
            }
            catch (Exception ex) {
                response.Message = ex.InnerException.ToString();
            }
            return response;
        }
    }
}
