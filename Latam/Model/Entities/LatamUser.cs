﻿namespace Latam.Model.Entities {
    public class LatamUser: LatamEntity {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
    }
}
