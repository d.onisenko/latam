﻿using System;

namespace Latam.Model.Entities {
    public class VisitorsCount: LatamEntity {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int Count { get; set; }
    }
}
