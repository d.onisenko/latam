﻿namespace Latam.Model.Entities {
    public class Shop: LatamEntity {
        public int Id { get; set; }
        public string NameEn { get; set; }
        public string NameUk { get; set; }
    }
}
