﻿namespace Latam.Model.Entities {
    public class ProductInOrder : LatamEntity {
        public int Id { get; set; }
        public int? OrderId { get; set; }
        public int? ProductId { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }

        public virtual Product Product { get; set; }
    }
}
