﻿using System;
using System.Collections.Generic;

namespace Latam.Model.Entities {
    public class Product : LatamEntity {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; } 
        public string NameUk { get; set; }
        public string NameEn { get; set; }
        public string DescriptionUk { get; set; }
        public string DescriptionEn { get; set; }
        public decimal Price { get; set; }
        public int CategoryId { get; set; }
        public string TopNotesUk { get; set; }
        public string MiddleNotesUk { get; set; }
        public string BaseNotesUk { get; set; }
        public string TopNotesEn { get; set; }
        public string MiddleNotesEn { get; set; }
        public string BaseNotesEn { get; set; }

        public virtual Category Category { get; set; }
        public virtual ICollection<ProductInPurpose> ProductInPurpose { get; set; }
    }
}
