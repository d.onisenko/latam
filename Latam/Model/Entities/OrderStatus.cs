﻿using System.Collections.Generic;

namespace Latam.Model.Entities {
    public class OrderStatus : LatamEntity {
        public int Id { get; set; }
        public string NameUk { get; set; }
        public string NameEn { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
