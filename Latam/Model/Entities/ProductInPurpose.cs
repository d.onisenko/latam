﻿namespace Latam.Model.Entities {
    public class ProductInPurpose {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int PurposeId { get; set; }

        public virtual Purpose Purpose { get; set; }
    }
}
