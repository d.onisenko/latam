﻿using System;
using System.Collections.Generic;

namespace Latam.Model.Entities {
    public class Order : LatamEntity {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? OrderStatusId { get; set; }
        public decimal TotalPrice { get; set; }
        public bool IsVisa { get; set; }
        public bool IsCash { get; set; }
        public string ContactName { get; set; }
        public string ContactSurname { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string Comment { get; set; }
        public bool IsPickup { get; set; }
        public bool IsNpPickup { get; set; }
        public bool IsNpCourier { get; set; }
        public int? PickupShopId { get; set; }
        public string City { get; set; }
        public string NpPickupNumber { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public string Flat { get; set; }

        public virtual OrderStatus OrderStatus { get; set; }
        public virtual Shop PickupShop { get; set; }
        public virtual List<ProductInOrder> ProductInOrder { get; set; }
    }
}
