﻿namespace Latam.Model.Entities {
    public class Purpose: LatamEntity {
        public int Id { get; set; }
        public string NameUk { get; set; }
        public string NameEn { get; set; }
    }
}
