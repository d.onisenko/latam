﻿using Latam.Model.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Latam.Model {
    public class LatamContext : DbContext {
        public LatamContext(DbContextOptions<LatamContext> options) : base(options) {

        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductInPurpose> ProductInPurposes { get; set; }
        public virtual DbSet<Purpose> Purposes { get; set; }
        public virtual DbSet<Shop> Shop { get; set; }
        public virtual DbSet<ProductInOrder> ProductInOrder { get; set; }
        public virtual DbSet<OrderStatus> OrderStatus { get; set; }
        public virtual DbSet<Order> Order { get; set; }
        public virtual DbSet<LatamUser> LatamUser { get; set; }
        public virtual DbSet<VisitorsCount> VisitorsCount { get; set; }

        //Scaffold-DbContext "Server=.\SQLEXPRESS;Database=Latam;Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer -Force -Context LatamContext -OutputDir ModelsTmp -Verbose

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.HasAnnotation("Relational:Collation", "Cyrillic_General_CI_AS");
            var cascadeFKs = modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetForeignKeys())
                .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascadeFKs) {
                fk.DeleteBehavior = DeleteBehavior.Restrict;
            }

            modelBuilder.Entity<Category>(entity => {
                entity.ToTable("Category");

                entity.Property(e => e.NameEn)
                    .IsRequired()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.NameUk)
                    .IsRequired()
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<Product>(entity => {
                entity.ToTable("Product");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DescriptionEn)
                    .IsRequired()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DescriptionUk)
                    .IsRequired()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.NameEn)
                    .IsRequired()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.NameUk)
                    .IsRequired()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<Purpose>(entity => {
                entity.ToTable("Purpose");
            });

            modelBuilder.Entity<ProductInPurpose>(entity => {
                entity.ToTable("ProductInPurpose");
            });

            modelBuilder.Entity<Shop>(entity => {
                entity.ToTable("Shop");

                entity.Property(e => e.NameEn)
                    .IsRequired()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.NameUk)
                    .IsRequired()
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<Order>(entity => {
                entity.ToTable("Order");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ContactEmail)
                    .IsRequired()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ContactName)
                    .IsRequired()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ContactPhone)
                    .IsRequired()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ContactSurname)
                    .IsRequired()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Flat)
                    .IsRequired()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.House)
                    .IsRequired()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.NpPickupNumber)
                    .IsRequired()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Street)
                    .IsRequired()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TotalPrice).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.OrderStatus)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.OrderStatusId);
            });

            modelBuilder.Entity<OrderStatus>(entity => {
                entity.ToTable("OrderStatus");
            });

            modelBuilder.Entity<ProductInOrder>(entity => {
                entity.ToTable("ProductInOrder");

                entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<LatamUser>(entity => {
                entity.ToTable("LatamUser");

                entity.Property(e => e.Name)
                    .IsRequired();

                entity.Property(e => e.Password)
                    .IsRequired();
            });

            modelBuilder.Entity<VisitorsCount>(entity => {
                entity.ToTable("VisitorsCount");

                entity.Property(e => e.Date)
                    .IsRequired();

                entity.Property(e => e.Count)
                    .IsRequired();
            });
        }
    }
}
