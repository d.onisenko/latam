﻿namespace Latam.Model.Dto {
    public class BaseWebResponse {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
