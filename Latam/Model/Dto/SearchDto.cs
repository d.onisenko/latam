﻿namespace Latam.Model.Dto {
    public class SearchDto {
        public int Id { get; set; }
        public string SectionName { get; set; }
        public string NameEn { get; set; }
        public string NameUk { get; set; }
    }
}
