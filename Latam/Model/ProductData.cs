﻿using Latam.Model.Entities;
using System.Collections.Generic;

namespace Latam.Model {
    public class ProductData {
        public List<Product> Products { get; set; }
        public List<Category> Categories { get; set; }
        public List<Purpose> Purposes { get; set; }
    }
}
