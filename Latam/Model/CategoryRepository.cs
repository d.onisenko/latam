﻿using Latam.Model.Entities;
using Latam.Repository;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Latam.Model {
    public class CategoryRepository {
        private readonly IUnitOfWork _unitOfWork;

        public CategoryRepository(IUnitOfWork unitOfWork) {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Category> Get() => _unitOfWork.Category.AsNoTracking();
    }
}
