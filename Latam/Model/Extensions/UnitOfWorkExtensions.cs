﻿using Latam.Model.Utils;
using Latam.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Latam.Model.Extensions {
    public static class UnitOfWorkExtensions {
        private static void EnsureConnectionOpen(this IDbConnection connection) {
            lock (connection) {
                if (connection.State == ConnectionState.Closed) {
                    connection.Open();
                }
            }
        }

        public static IDbCommand LoadStoredProc(this IUnitOfWork unitOfWork, string storedProcName) {
            var cmd = unitOfWork.CreateDbConnection().CreateCommand();
            cmd.CommandText = storedProcName;
            cmd.CommandType = CommandType.StoredProcedure;
            return cmd;
        }

        public static IDbCommand LoadCommand(this IUnitOfWork unitOfWork, string commandText) {
            var cmd = unitOfWork.CreateDbConnection().CreateCommand();
            cmd.CommandText = commandText;
            cmd.CommandType = CommandType.Text;
            return cmd;
        }

        public static IDbCommand WithParam(this IDbCommand cmd, string paramName, object paramValue) {
            if (string.IsNullOrEmpty(cmd.CommandText))
                throw new InvalidOperationException("Call LoadStoredProc before using this method");

            var param = cmd.CreateParameter();
            param.ParameterName = paramName;
            param.Value = paramValue;
            cmd.Parameters.Add(param);
            return cmd;
        }

        public static IDbCommand WithParam(this IDbCommand cmd, string paramName, object paramValue, DbType type) {
            if (string.IsNullOrEmpty(cmd.CommandText))
                throw new InvalidOperationException("Call LoadStoredProc before using this method");

            var param = cmd.CreateParameter();
            param.ParameterName = paramName;
            param.DbType = type;
            param.Value = paramValue;
            cmd.Parameters.Add(param);
            return cmd;
        }

        public static IDbCommand WithParam(this IDbCommand cmd, IDictionary<string, object> paramValues) {
            foreach (var param in paramValues)
                cmd.WithParam(param.Key, param.Value);
            return cmd;
        }


        public static IDbCommand WithOutParam(this IDbCommand cmd, string paramName, DbType type, int size = 0) {
            if (string.IsNullOrEmpty(cmd.CommandText))
                throw new InvalidOperationException("Call LoadStoredProc before using this method");

            var param = cmd.CreateParameter();
            param.ParameterName = paramName;
            param.Direction = ParameterDirection.Output;
            if (size > 0)
                param.Size = size;
            param.DbType = type;
            cmd.Parameters.Add(param);
            return cmd;
        }

        public static IList<T> MapToList<T>(this IDataReader reader) {
            var type = typeof(T);
            if (type == typeof(bool) || type == typeof(short) || type == typeof(int) || type == typeof(long) ||
                type == typeof(decimal) || type == typeof(string) || type == typeof(DateTime)) {
                return MapToListSimple<T>(reader);
            }
            return MapToListComplex<T>(reader);
        }

        private static IList<T> MapToListSimple<T>(IDataReader reader) {
            var objList = new List<T>();
            while (reader.Read()) {
                objList.Add(ConvertUtils.ValueAsType<T>(reader.GetValue(0)));
            }
            return objList;
        }

        private static IList<T> MapToListComplex<T>(IDataReader reader) {
            var objList = new List<T>();
            var props = typeof(T).GetRuntimeProperties();

            var colMapping = Enumerable
                .Range(0, reader.FieldCount)
                .Select(x => new {
                    Name = reader.GetName(x),
                    Index = x
                })
                .Where(x => props.Any(y => string.Equals(y.Name, x.Name, StringComparison.OrdinalIgnoreCase)))
                .ToDictionary(key => key.Name.ToLower());

            while (reader.Read()) {
                var obj = Activator.CreateInstance<T>();
                foreach (var prop in props) {
                    var propertyName = prop.Name.ToLower();
                    if (!colMapping.ContainsKey(propertyName))
                        continue;

                    var value = reader.GetValue(colMapping[propertyName].Index);
                    if (prop.PropertyType == typeof(Guid) || prop.PropertyType == typeof(Guid?)) {
                        Guid.TryParse(value == DBNull.Value ? string.Empty : value.ToString(), out var gValue);
                        prop.SetValue(obj, gValue);
                    }
                    else {
                        prop.SetValue(obj, value == DBNull.Value ? null : value);
                    }
                }
                objList.Add(obj);
            }
            return objList;
        }

        public static IList<T> Execute<T>(this IDbCommand command) {
            using (command) {
                command.Connection.EnsureConnectionOpen();
                try {
                    using (var reader = command.ExecuteReader()) {
                        if (typeof(T) == typeof(DataTable)) {
                            var obj = Activator.CreateInstance<T>();
                            (obj as DataTable).Load(reader);
                            return new List<T> { obj };
                        }
                        return reader.MapToList<T>();
                    }
                }
                finally {
                    command.Connection.Close();
                }
            }
        }

        public static IList<object> Execute(this IDbCommand command, Type type) {
            var result = new List<Object>();
            using (command) {
                command.Connection.EnsureConnectionOpen();
                try {
                    using (var reader = command.ExecuteReader()) {
                        while (reader.Read()) {
                            var values = Enumerable.Range(0, reader.FieldCount)
                                .Select(x => reader.GetName(x))
                                .ToDictionary(
                                    x => x,
                                    x => {
                                        var value = reader[x];
                                        return value == DBNull.Value ? null : value;
                                    });
                            var item = Activator.CreateInstance(type);
                            MapToItem(item, values);
                            result.Add(item);
                        }
                    }
                }
                finally {
                    command.Connection.Close();
                }
            }
            return result;
        }

        private static void MapToItem(object entity, Dictionary<string, object> values) {
            var type = entity.GetType();
            var properties = type
                .GetRuntimeProperties()
                .ToDictionary(x => x.Name, x => x);

            foreach (var value in values.ToList()) {
                if (properties.TryGetValue(value.Key, out var property)) {
                    property.SetValue(entity, value.Value);
                    values.Remove(value.Key);
                }
            }

            var referencePropertyNames = values
                .GroupBy(x => x.Key.Split(new[] { '.' }, 2)[0])
                .Select(x => x.Key)
                .ToList();

            foreach (var referencePropertyName in referencePropertyNames) {
                var referenceProperty = properties[referencePropertyName];
                var referenceEntity = Activator.CreateInstance(referenceProperty.PropertyType);
                var referenceValues = values
                    .Where(p => p.Key.Split(new[] { '.' }, 2)[0] == referencePropertyName)
                    .ToDictionary(
                        x => x.Key.Substring(referencePropertyName.Length + 1),
                        x => x.Value);
                MapToItem(referenceEntity, referenceValues);
                referenceProperty.SetValue(entity, referenceEntity);
            }
        }

        public static int ExecuteNonSelect(this IDbCommand command) {
            using (command) {
                command.Connection.EnsureConnectionOpen();
                try {
                    return command.ExecuteNonQuery();
                }
                finally {
                    command.Connection.Close();
                }
            }
        }

        public static T ExecuteScalar<T>(this IDbCommand command) {
            using (command) {
                command.Connection.EnsureConnectionOpen();
                try {
                    var result = command.ExecuteScalar();
                    return (T)Convert.ChangeType(result, typeof(T));
                }
                finally {
                    command.Connection.Close();
                }
            }
        }

        public static IDictionary<string, object> ExecuteWithOutputParams(this IDbCommand command) {
            using (command) {
                command.Connection.EnsureConnectionOpen();
                try {
                    var result = command.ExecuteNonQuery();
                    var output = new Dictionary<string, object>();
                    foreach (DbParameter param in command.Parameters) {
                        if (param.Direction == ParameterDirection.Output ||
                            param.Direction == ParameterDirection.InputOutput ||
                            param.Direction == ParameterDirection.ReturnValue) {
                            output.Add(param.ParameterName, param.Value == DBNull.Value ? null : param.Value);
                        }
                    }
                    return output;
                }
                finally {
                    command.Connection.Close();
                }
            }
        }

        public static DataTable Execute(this IDbCommand command) {
            using (command) {
                command.Connection.EnsureConnectionOpen();
                try {
                    using var reader = command.ExecuteReader();
                    var dt = Activator.CreateInstance<DataTable>();
                    dt.Load(reader);
                    return dt;
                }
                finally {
                    command.Connection.Close();
                }
            }
        }

        public static DataSet ExecuteToDataSet(this IDbCommand command) {
            using (command) {
                command.Connection.EnsureConnectionOpen();
                try {
                    var dataSet = Activator.CreateInstance<DataSet>();
                    using var adapter = new SqlDataAdapter {
                        SelectCommand = (SqlCommand)command
                    };
                    adapter.Fill(dataSet, "Table");
                    return dataSet;
                }
                finally {
                    command.Connection.Close();
                }
            }
        }
    }
}
