using Latam.LocalizerService;
using Latam.Middleware;
using Latam.Model;
using Latam.Model.Utils;
using Latam.Repository;
using Latam.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Latam {
    public class Startup {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration) {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services) {
            services.AddMvc().AddNewtonsoftJson();
            services.AddControllersWithViews();

            services.AddSpaStaticFiles(configuration => {
                configuration.RootPath = "ClientApp/dist";
            });

            services.AddControllers()
                .AddControllersAsServices()
                .AddNewtonsoftJson(options =>
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                );

            services.AddAuthentication("BasicAuthentication")
              .AddScheme<AuthenticationSchemeOptions, AuthHandler>("BasicAuthentication", null);
            services.AddSingleton<IAuthService, AuthService>();

            services.AddDbContext<LatamContext>(
                options => options.UseSqlServer(_configuration.GetConnectionString("LatamDb")),
                ServiceLifetime.Transient,
                ServiceLifetime.Transient);

            //singleton
            services.AddSingleton<ILocalizer, Localizer>();
            services.AddSingleton<ICryptoUtils, CryptoUtils>();

            //transient
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<ProductRepository>();
            services.AddTransient<ShopRepository>();
            services.AddTransient<CategoryRepository>();
            services.AddTransient<SearchService>();
            services.AddTransient<OrderService>();
            services.AddTransient<TelegramService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }
            else {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseMiddleware(typeof(VisitorMiddleware));

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            if (!env.IsDevelopment()) {
                app.UseSpaStaticFiles();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa => {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment()) {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}
