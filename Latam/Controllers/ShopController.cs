﻿using Latam.Model;
using Latam.Model.Dto;
using Latam.Model.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Latam.Controllers {
    [ApiController]
    [Route("[controller]/[action]")]
    public class ShopController: ControllerBase {
        private readonly ShopRepository _shopRepository;

        public ShopController(ShopRepository shopRepository) {
            _shopRepository = shopRepository;
        }

        [HttpGet]
        public IEnumerable<Shop> Get() => _shopRepository.Get();

        [HttpPost]
        public BaseWebResponse Remove(IEnumerable<Shop> shops) => _shopRepository.Remove(shops);

        [HttpPost]
        public BaseWebResponse Edit(IEnumerable<Shop> shops) => _shopRepository.Edit(shops);
    }
}
