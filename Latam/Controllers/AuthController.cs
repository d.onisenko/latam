﻿using Latam.Model;
using Latam.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Latam.Controllers {
    [ApiController]
    [AllowAnonymous]
    [Route("[controller]/[action]")]
    public class AuthController: ControllerBase {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService) {
            _authService = authService;
        }

        [HttpPost]
        public AuthUser Login(AuthenticateModel authenticate) => _authService.Authenticate(authenticate);
    }
}
