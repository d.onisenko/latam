﻿using Latam.LocalizerService;
using Microsoft.AspNetCore.Mvc;

namespace Latam.Controllers {
    [ApiController]
    [Route("[controller]/[action]")]
    public class TranslationController : ControllerBase {

        private ILocalizer _localizer;

        public TranslationController(ILocalizer localizer) {
            _localizer = localizer;
        }

        [HttpGet]
        public LocalizerResponse GetAll() => _localizer.GetAll();

        [HttpPost]
        public void SetCulture(SetCultureRequest setCultureRequest) {
            _localizer.SetCulture(setCultureRequest.Culture);
        }
    }
}
