﻿using Latam.Model;
using Latam.Model.Dto;
using Latam.Model.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Latam.Controllers {
    [ApiController]
    [Route("[controller]/[action]")]
    public class ProductController : ControllerBase {
        private readonly ProductRepository _productRepository;

        public ProductController(ProductRepository productRepository) {
            _productRepository = productRepository;
        }

        [HttpGet]
        public IEnumerable<Product> Get() => _productRepository.Get();

        [HttpGet]
        public Product GetById(int id) => _productRepository.GetById(id);

        [HttpPost]
        public IEnumerable<Product> GetSeveralByIds(IEnumerable<int> ids) => _productRepository.GetSeveralByIds(ids);

        [HttpPost]
        public BaseWebResponse Edit(IEnumerable<Product> products) => _productRepository.Edit(products);
    }
}
