﻿using Latam.Model.Dto;
using Latam.Model.Entities;
using Latam.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Latam.Controllers {
    [ApiController]
    [Route("[controller]/[action]")]
    public class OrderController {
        private readonly OrderService _orderService;

        public OrderController(OrderService orderService) {
            _orderService = orderService;
        }

        [HttpPost]
        public BaseWebResponse Create(Order order) => _orderService.Create(order);

        [HttpGet]
        public IEnumerable<Order> Get() => _orderService.Get();

        [HttpGet]
        public IEnumerable<OrderStatus> GetStatuses() => _orderService.GetStatuses();

        [HttpPost]
        public void Save(IEnumerable<Order> orders) => _orderService.Save(orders);

        [HttpGet]
        public int GetVisitorCount() => _orderService.GetVisitorCount();
    }
}
