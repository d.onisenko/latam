﻿using Latam.Model.Dto;
using Latam.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Latam.Controllers {
    [ApiController]
    [Route("[controller]/[action]")]
    public class SearchController : ControllerBase {
        private readonly SearchService _searchService;

        public SearchController(SearchService searchService) {
            _searchService = searchService;
        }

        [HttpGet]
        public List<SearchDto> Execute(string searchValue) => _searchService.Execute(searchValue);
    }
}
