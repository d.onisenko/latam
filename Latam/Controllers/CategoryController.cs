﻿using Latam.Model;
using Latam.Model.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Latam.Controllers {
    [ApiController]
    [Route("[controller]/[action]")]
    public class CategoryController : ControllerBase {
        private readonly CategoryRepository _categoryRepository;

        public CategoryController(CategoryRepository categoryRepository) {
            _categoryRepository = categoryRepository;
        }

        [HttpGet]
        public IEnumerable<Category> Get() => _categoryRepository.Get();
    }
}
