﻿using Latam.Model.Dto;
using Latam.Model.Entities;
using Latam.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Latam.Services {
    public class OrderService {
        private readonly IUnitOfWork _unitOfWork;
        private readonly TelegramService _telegramService;

        public OrderService(IUnitOfWork unitOfWork, TelegramService telegramService) {
            _unitOfWork = unitOfWork;
            _telegramService = telegramService;
        }

        public BaseWebResponse Create(Order order) {
            var response = new BaseWebResponse();
            try {
                var status = _unitOfWork.OrderStatus.FirstOrDefault(s => s.NameEn == "New");
                if (status == null) {
                    throw new Exception("нет статуса New");
                }

                order.OrderStatusId = status.Id;
                order.CreatedOn = DateTime.Now;

                var notificationText = $"Кто-то заказал вонючку! " +
                    $"Заказ на сумму {order.TotalPrice}," +
                    $"товары: {string.Join(", ", order.ProductInOrder.Select(p => $"{p.Product.NameEn} - {p.Quantity}"))}";

                order.ProductInOrder.ForEach(p => p.Product = null);

                _unitOfWork.Order.Add(order);
                _telegramService.SendOrderNotification(notificationText);

                response.IsSuccess = true;
            } catch (Exception ex) {
                response.Message = ex.Message;
            }
            return response;
        }

        public IEnumerable<Order> Get() => _unitOfWork.Order
            .Include(o => o.ProductInOrder)
            .ThenInclude(p => p.Product)
            .Include(o => o.OrderStatus)
            .Include(o => o.PickupShop)
            .OrderByDescending(o => o.CreatedOn)
            .Take(100)
            .AsEnumerable();

        public IEnumerable<OrderStatus> GetStatuses() => _unitOfWork.OrderStatus
            .AsNoTracking()
            .AsEnumerable();

        public void Save(IEnumerable<Order> orders) {
            foreach (var order in orders) {
                order.ProductInOrder.ForEach(p => p.Product = null);
                order.OrderStatusId = order.OrderStatus.Id;
                order.PickupShopId = order.PickupShop.Id;
                order.OrderStatus = null;
                order.PickupShop = null;
                _unitOfWork.Order.Update(order);
            }
        }

        public int GetVisitorCount() => _unitOfWork.VisitorsCount.FirstOrDefault(x => x.Date == DateTime.Today)?.Count ?? 0;
    }
}
