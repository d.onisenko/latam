﻿namespace Latam.Services {
    public interface ICryptoUtils {
        string Encrypt(string value);
        string Decrypt(string value);
    }
}
