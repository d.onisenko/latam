﻿using Latam.Model.Dto;
using Latam.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Latam.Services {
    public class SearchService {
        private readonly IUnitOfWork _unitOfWork;

        public SearchService(IUnitOfWork unitOfWork) {
            _unitOfWork = unitOfWork;
        }

        public List<SearchDto> Execute(string searchValue) {
            var result = new List<SearchDto>();
            var products = _unitOfWork.Product
                .Include(p => p.Category)
                .Include(p => p.ProductInPurpose)
                .ThenInclude(pp => pp.Purpose)
                .AsNoTracking()
                .AsEnumerable()
                .Where(p =>
                    p.NameEn.Contains(searchValue, StringComparison.OrdinalIgnoreCase)
                    || p.NameUk.Contains(searchValue, StringComparison.OrdinalIgnoreCase)
                    || p.Category.NameEn.Contains(searchValue, StringComparison.OrdinalIgnoreCase)
                    || p.Category.NameUk.Contains(searchValue, StringComparison.OrdinalIgnoreCase)
                    || p.ProductInPurpose
                        .Any(pp => pp.Purpose.NameUk.Contains(searchValue, StringComparison.OrdinalIgnoreCase)
                                || pp.Purpose.NameEn.Contains(searchValue, StringComparison.OrdinalIgnoreCase)))
                .Select(s => new SearchDto {
                    Id = s.Id,
                    NameEn = s.NameEn,
                    NameUk = s.NameUk,
                    SectionName = "Product"
                });

            var categories = _unitOfWork.Category
                .AsNoTracking()
                .AsEnumerable()
                .Where(c => c.NameEn.Contains(searchValue, StringComparison.OrdinalIgnoreCase)
                         || c.NameUk.Contains(searchValue, StringComparison.OrdinalIgnoreCase))
                .Select(s => new SearchDto {
                    Id = s.Id,
                    NameEn = s.NameEn,
                    NameUk = s.NameUk,
                    SectionName = "Category"
                });

            result.AddRange(products);
            result.AddRange(categories);
            return result;
        }
    }
}
