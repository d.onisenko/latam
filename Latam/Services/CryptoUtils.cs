﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Latam.Services {
    public class CryptoUtils : ICryptoUtils {
        private readonly TripleDESCryptoServiceProvider _secureProvider;
        private readonly byte[] _initializationVectorBytes;
        private readonly byte[] _currentKeyBytes;

        public CryptoUtils(IConfiguration configuration) {
            var initializationVector = configuration["SecureText:InitializationVector"];
            var currentKey = configuration["SecureText:CurrentKey"];
            _secureProvider = new TripleDESCryptoServiceProvider();
            _initializationVectorBytes = Encoding.ASCII.GetBytes(initializationVector);
            _currentKeyBytes = Encoding.ASCII.GetBytes(currentKey);
        }

        public string Encrypt(string value) {
            ICryptoTransform encryptor;
            lock (_secureProvider) {
                encryptor = _secureProvider.CreateEncryptor(_currentKeyBytes, _initializationVectorBytes);
            }
            using (var encryptorMemoryStream = new MemoryStream())
            using (var encryptorCryptoStream = new CryptoStream(encryptorMemoryStream, encryptor, CryptoStreamMode.Write)) {
                var base64DecryptedText = Convert.ToBase64String(Encoding.Unicode.GetBytes(value));
                var base64DecryptedTextBytes = Encoding.ASCII.GetBytes(base64DecryptedText);
                encryptorCryptoStream.Write(base64DecryptedTextBytes, 0, base64DecryptedTextBytes.Length);
                encryptorCryptoStream.FlushFinalBlock();
                return Convert.ToBase64String(encryptorMemoryStream.ToArray());
            }
        }

        public string Decrypt(string value) {
            if (string.IsNullOrEmpty(value)) {
                return null;
            }
            ICryptoTransform decryptor;
            lock (_secureProvider) {
                decryptor = _secureProvider.CreateDecryptor(_currentKeyBytes, _initializationVectorBytes);
            }
            using (var decryptorMemoryStream = new MemoryStream())
            using (var decryptorCryptoStream = new CryptoStream(decryptorMemoryStream, decryptor, CryptoStreamMode.Write)) {
                var encryptedTextBytes = Convert.FromBase64String(value);
                decryptorCryptoStream.Write(encryptedTextBytes, 0, encryptedTextBytes.Length);
                decryptorCryptoStream.FlushFinalBlock();
                var base64DecryptedText = Encoding.ASCII.GetString(decryptorMemoryStream.ToArray());
                return Encoding.Unicode.GetString(Convert.FromBase64String(base64DecryptedText));
            }
        }
    }
}
