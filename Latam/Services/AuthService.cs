﻿using Latam.Model;
using Latam.Model.Utils;
using Latam.Repository;
using Microsoft.EntityFrameworkCore;
using System.Collections.Concurrent;
using System.Linq;

namespace Latam.Services {
    public interface IAuthService {
        AuthUser Authenticate(AuthenticateModel authenticate);
    }
    public class AuthService : IAuthService {
        readonly IUnitOfWork _unitOfWork = null;
        readonly ICryptoUtils _cryptoUtils = null;
        readonly ConcurrentDictionary<string, AuthUser> _users = new ConcurrentDictionary<string, AuthUser>();
        public AuthService(IUnitOfWork unitOfWork, ICryptoUtils cryptoUtils) {
            _unitOfWork = unitOfWork;
            _cryptoUtils = cryptoUtils;
        }

        private AuthUser GetUser(AuthenticateModel authenticate) {
            var pass = _cryptoUtils.Encrypt(authenticate.Password);
            return _unitOfWork.LatamUser.AsNoTracking().Where(e => e.Name == authenticate.Username && e.Password == pass)
                .Select(x => new AuthUser {
                    Id = x.Id,
                    Login = x.Name,
                    Password = x.Password,
                    IsAdmin = x.IsAdmin
                }).FirstOrDefault();
        }

        public AuthUser Authenticate(AuthenticateModel authenticate) {
            if (!_users.TryGetValue(authenticate.Username, out AuthUser user)) {
                user = GetUser(authenticate);
            }
            if (user == null) {
                return null;
            }
            _users[user.Login] = user;
            return user.WithoutPassword();
        }
    }
}
