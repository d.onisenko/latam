﻿using Latam.Model.Entities;
using Latam.Repository;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Latam.Middleware {
    public class VisitorMiddleware {
        private readonly RequestDelegate _next;
        private readonly IUnitOfWork _unitOfWork;

        public VisitorMiddleware(RequestDelegate next, IUnitOfWork unitOfWork) {
            _next = next;
            _unitOfWork = unitOfWork;
        }

        public async Task InvokeAsync(HttpContext context) {
            string visitorId = context.Request.Cookies["VisitorId"];
            if (visitorId == null) {
                try {
                    AddCount();
                } catch (Exception) {
                    //ignore
                }

                context.Response.Cookies.Append("VisitorId", Guid.NewGuid().ToString(), new CookieOptions() {
                    Path = "/",
                    HttpOnly = true,
                    Secure = false,
                });
            }

            await _next(context);
        }

        private void AddCount() {
            var visitorCount = _unitOfWork.VisitorsCount.FirstOrDefault(v => v.Date == DateTime.Today);
            if (visitorCount != null) {
                visitorCount.Count += 1;
                _unitOfWork.VisitorsCount.Update(visitorCount);
            } else {
                var newVisitorCount = new VisitorsCount { Count = 1, Date = DateTime.Today };
                _unitOfWork.VisitorsCount.Add(newVisitorCount);
            }
        }
    }
}
