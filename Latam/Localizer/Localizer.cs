﻿using Newtonsoft.Json.Linq;
using System.Globalization;
using System.Resources;

namespace Latam.LocalizerService {
    public class Localizer : ILocalizer {

        private ResourceManager _resourceManager;
        private LocalizerResponse _resources;

        private void Initialize() {
            _resources = new LocalizerResponse();
            _resourceManager = new ResourceManager("Latam.Resource", typeof(Resource).Assembly);
            foreach (var culture in new[] { "en", "uk" }) {
                var resourceSet = _resourceManager.GetResourceSet(new CultureInfo(culture), true, true);
                var resourceObject = new JObject();
                var enumerator = resourceSet.GetEnumerator();
                while (enumerator.MoveNext()) {
                    resourceObject.Add(enumerator.Key.ToString(), enumerator.Value.ToString());
                }
                _resources.Items.Add(new LocalizerItem { Culture = culture, Dictionary = resourceObject });
            }
        }

        public void SetCulture(string culture) {
            CultureInfo.CurrentCulture = new CultureInfo(culture);
        }

        public string Translate(string input) {
            return _resourceManager.GetString(input, CultureInfo.CurrentCulture);
        }

        public LocalizerResponse GetAll() {
            if (_resources == null) {
                Initialize();
            }
            return _resources;
        }

        internal class Resource { }
    }
}
