﻿namespace Latam.LocalizerService {
    public class SetCultureRequest {
        public string Culture { get; set; }
    }
}
