﻿namespace Latam.LocalizerService {
    public interface ILocalizer {
        string Translate(string input);
        LocalizerResponse GetAll();
        void SetCulture(string culture);
    }
}
