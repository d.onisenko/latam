﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Latam.LocalizerService {
    public class LocalizerResponse {
        public LocalizerResponse() {
            Items = new List<LocalizerItem>();
        }
        public List<LocalizerItem> Items { get; set; }
    }

    public class LocalizerItem {
        public string Culture { get; set; }
        public JObject Dictionary { get; set; }
    }
}
